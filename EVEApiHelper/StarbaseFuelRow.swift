//
//  StarbaseFuelRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/27/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class StarbaseFuelRow: GenericRowDescription {
    
    var quantity: Int? {
        return getIntForKey("quantity")
    }
    
    var typeID: Int64? {
        return getInt64ForKey("typeID")
    }
   
}

//
//  AllianceTableViewCell.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/25/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class AllianceTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var allianceNameLabel: UILabel!
    @IBOutlet weak var allianceDateLabel: UILabel!
    @IBOutlet weak var allianceNumbersLabel: UILabel!
    @IBOutlet var allianceTickerLabel: UILabel!
    @IBOutlet weak var allianceImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}

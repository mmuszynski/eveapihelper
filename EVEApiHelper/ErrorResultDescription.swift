//
//  ErrorResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/17/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class ErrorResultDescription: ResultDescription {
    
    var rawError = ""
    var rawErrorCode = ""
    
    var error: String {
        get {
            return rawError
        }
    }
    
    var errorCode: Int? {
        get {
            return Int(rawErrorCode)
        }
    }
    
    var errorAsNSError: NSError {
        get {
            return NSError(domain: "com.mmuszynski.EVEAPIHelper.errors", code: self.errorCode!, userInfo: [NSLocalizedDescriptionKey : self.error])
        }
    }

}

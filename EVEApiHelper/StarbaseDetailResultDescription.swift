//
//  StarbaseDetailResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/27/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class StarbaseDetailResultDescription: ResultDescription {
   
    var itemID: Int64? {
        return getInt64ForKey("itemID")
    }
    var allowAllianceMembers: Bool {
        return getIntForKey("allowAllianceMembers") == 1
    }
    var allowCorporationMembers: Bool {
        return getIntForKey("allowCorporationMembers") == 1
    }
    var deployFlags: Int? {
        return getIntForKey("deployFlags")
    }
    var usageFlags: Int? {
        return getIntForKey("usageFlags")
    }
    var state: Int? {
        return getIntForKey("state")
    }
    var onlineTimestamp: NSDate? {
        return getDateForKey("onlineTimestamp")
    }
    var stateTimestamp: NSDate? {
        return getDateForKey("stateTimestamp")
    }
    
    var fuelRows: [StarbaseFuelRow] {
        if let rows = rowSets["fuel"]?.rows as? [StarbaseFuelRow] {
            return rows
        }
        
        return []
    }
    
}

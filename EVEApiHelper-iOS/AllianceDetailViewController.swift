//
//  AllianceDetailViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/26/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class AllianceDetailViewController: UIViewController, UITableViewDataSource, GenericXMLFetcherDelegate, ImageServerFetcherDelegate {
    
    var alliance : AllianceRow!
    var allianceImage : UIImage?
    
    var corpImages = [Int : UIImage]()
    var corpRows = [Int : CorporationRow]()
    var corpFetchers = [Int : GenericXMLFetcher]()
    var imageFetchers = [Int : ImageServerFetcher]()
    var corpIDs = [Int]()
    var corpIDtoIndexPath = [Int : NSIndexPath]()
    
    @IBOutlet weak var allianceNameLabel: UILabel!
    @IBOutlet weak var allianceStartDateLabel: UILabel!
    @IBOutlet weak var allianceNumbersLabel: UILabel!
    @IBOutlet weak var allianceImageView: UIImageView!
    @IBOutlet weak var allianceCorpNumbersLabel: UILabel!
    
    @IBOutlet weak var memberCorpTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        allianceImageView.image = allianceImage
        allianceNameLabel.text = alliance.name + " - [\(alliance.shortName)]"
        
        let membership = alliance.memberCount
        let membershipString = "\(membership!) " + simplePluralize("member", membership!)
        allianceNumbersLabel.text = membershipString
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-YYYY"
        let startDateString = dateFormatter.stringFromDate(alliance.startDate!)
        allianceStartDateLabel.text = "Established: " + startDateString
        
        let corps = alliance.memberCorporations.count
        let corpString = "\(corps) " + simplePluralize("corporation", corps)
        allianceCorpNumbersLabel.text = corpString
        
        corpIDs = alliance.memberCorporations.map { $0.corporationID! }
        let executorID = alliance.executorCorpID!
        if let executorPosition = find(corpIDs, executorID) {
            corpIDs.removeAtIndex(executorPosition)
            corpIDs.insert(executorID, atIndex: 0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsIntableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return corpIDs.count - 1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Executor Corporation"
        }
        return "Member Corporations"
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = memberCorpTableView.dequeueReusableCellWithIdentifier("corpCell") as! CorporationTableViewCell
        
        let corpID = corpIDs[indexPath.row + indexPath.section]
        corpIDtoIndexPath[corpID] = indexPath
        
        if let image = corpImages[corpID] {
            cell.corpImageView.image = image
            cell.spinner.stopAnimating()
        } else {
            cell.corpImageView.image = nil
            cell.spinner.startAnimating()
        }
        
        if let corpRow = corpRows[corpID] {
            cell.corpNameLabel.text = corpRow.corporationName
            if let members = corpRow.memberCount {
                let memberString = "\(members) " + simplePluralize("member", members)
                cell.corpNumbersLabel.text = memberString
            }
        } else {
            cell.corpNameLabel.text = "Loading..."
            cell.corpNumbersLabel.text = nil
            
            let apiCall = EVEApiHelperCall(callGroup: .Corporation, callType: .CorporationSheet)
            if corpFetchers[corpID] == nil {
                if let fetcher = GenericXMLFetcher(apiCall: apiCall, error: nil) {
                    fetcher.attributes = ["corporationID" : corpID]
                    fetcher.delegate = self
                    fetcher.start()
                } else {
                    prIntln("problem with the fetcher")
                }
            }
        }
        
        return cell
        
    }
    
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?) {

    }
    
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        if let corpID = fetcher.attributes?["corporationID"] as? Int {
            if let indexPath = corpIDtoIndexPath[corpID] {
                if let result = fetcher.result as? CorporationSheetResultDescription {
                    corpRows[corpID] = result.corporationRow
                }
            }
            fetchImageForCorpID(corpID)
            corpFetchers[corpID] = nil
        }
        
    }
    
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
    }
    
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {
        
    }
    
    func fetchImageForCorpID(id: Int) {
        if imageFetchers[id] == nil {
            let imageFetcher = ImageServerFetcher(assetType: .Corporation, assetID: id, assetWidth: 128)
            imageFetchers[id] = imageFetcher
            imageFetcher.delegate = self
            imageFetcher.fetch()
        }
    }
    
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: UIImage) {
        let id = fetcher.assetID
        imageFetchers[id] = nil
        corpImages[id] = image
        if let path = corpIDtoIndexPath[id] {
            dispatch_async(dispatch_get_main_queue(), {
                self.memberCorpTableView.reloadRowsAtIndexPaths([path], withRowAnimation: .None)
            })
        }
    }
    
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher) {
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let cell = sender as? CorporationTableViewCell {
            if let index = memberCorpTableView.indexPathForCell(cell) {
                let corpIndex = corpIDs[index.row + index.section]
                let corp = corpRows[corpIndex]
                if let dest = segue.destinationViewController as? CorporationDetailViewController {
                    dest.corporation = corp
                }
            }
            
        }

    }

}

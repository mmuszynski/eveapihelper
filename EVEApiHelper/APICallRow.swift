//
//  APICallRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/19/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class APICallRow: GenericRowDescription {
    
    var accessMask : Int? {
        get {
            let mask = self.rawAttributes["accessMask"] as! String
            return Int(mask)
        }
    }
    
    var type : String {
        get {
            return self.rawAttributes["type"] as! String
        }
    }
    
    var name : String {
        get {
            return self.rawAttributes["name"] as! String
        }
    }
    
    var groupID : Int? {
        get {
            let id = self.rawAttributes["groupID"] as! String
            return Int(id)
        }
    }
    
    var desc : String {
        get {
            return self.rawAttributes["description"] as! String
        }
    }

}

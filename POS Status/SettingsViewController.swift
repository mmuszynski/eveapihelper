//
//  SettingsViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 3/5/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    
    @IBOutlet weak var fuelThresholdSlider: UISlider!
    @IBOutlet weak var reactionInputThresholdSlider: UISlider!
    @IBOutlet weak var reactionOutputThresholdSlider: UISlider!
    
    @IBOutlet weak var fuelThresholdLabel: UILabel!
    @IBOutlet weak var reactionInputThresholdLabel: UILabel!
    @IBOutlet weak var reactionoutputThresholdLabel: UILabel!
    
    @IBAction func sliderValueChanged(sender: AnyObject) {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        var fuel: Float
        var input: Float
        var output: Float
        
        if sender is UITableViewController {
            fuel = defaults.floatForKey("com.mmuszynski.POSStatus.fuelThreshold")
            input = defaults.floatForKey("com.mmuszynski.POSStatus.inputThreshold")
            output = defaults.floatForKey("com.mmuszynski.POSStatus.outputThreshold")
            
            fuelThresholdSlider.value = fuel
            reactionInputThresholdSlider.value = input
            reactionOutputThresholdSlider.value = output
        }
        
        let formatter = NSNumberFormatter()
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        formatter.roundingIncrement = 1
        
        fuel = floor(fuelThresholdSlider.value)
        fuelThresholdLabel.text = formatter.stringFromNumber(fuel)! + " " + simplePluralize("day", count: fuel)
        
        formatter.numberStyle = NSNumberFormatterStyle.PercentStyle
        input = reactionInputThresholdSlider.value
        reactionInputThresholdLabel.text = formatter.stringFromNumber(input)
        output = reactionOutputThresholdSlider.value
        reactionoutputThresholdLabel.text = formatter.stringFromNumber(output)
        
        defaults.setFloat(fuel, forKey: "com.mmuszynski.POSStatus.fuelThreshold")
        defaults.setFloat(input, forKey: "com.mmuszynski.POSStatus.inputThreshold")
        defaults.setFloat(output, forKey: "com.mmuszynski.POSStatus.outputThreshold")
        
    }
    
    func simplePluralize<T>(word: String, count: T) -> String {
        if let num = count as? Int {
            return num == 1 ? word : word + "s"
        } else if let num = count as? Float {
            return (num >= 1.0 && num < 2.0) ? word : word + "s"
        } else if let num = count as? Double {
            return (num >= 1.0 && num < 2.0) ? word : word + "s"
        }
        
        return word
    }
    
    @IBAction func finalizeSliderValue(sender: AnyObject) {
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        sliderValueChanged(self)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableView.bounds.size.height / 5.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ImageServerFetcher.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/23/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

extension Int {
    var minute: Int     { get { return 60 * self } }
    var minutes: Int    { get { return self.minute } }
    var hour: Int       { get { return 60 * self.minute } }
    var hours: Int      { get { return self.hour } }
    var day: Int        { get { return 24 * self.hour } }
    var days: Int       { get { return self.day } }
    var week: Int       { get { return 7 * self.day } }
    var weeks: Int      { get { return self.week } }
    var month: Int      { get { return 30 * self.day } }
    var months: Int     { get { return self.month } }
    var year: Int       { get { return 365 * self.day } }
    var years: Int      { get { return self.year } }
}

protocol ImageServerFetcherDelegate {
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: UIImage)
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher)
}

class ImageServerFetcher: NSObject, NSURLSessionDataDelegate {
    
    let assetType : AssetType
    let assetID : Int
    let assetWidth: Int
    var url : NSURL?
    var cacheTimeInSeconds = 1.week
    
    var image : UIImage?
    var downloadData = NSMutableData()
    var delegate : ImageServerFetcherDelegate?
    
    var filename : String {
        get {
            return "\(assetID)_\(assetWidth).\(fileType)"
        }
    }
    
    enum AssetType : String  {
        case Alliance = "Alliance"
        case Corporation = "Corporation"
        case Character = "Character"
        case TypeID = "Type"
        case Render = "Render"
    }
    
    var fileType : String {
        get {
            switch self.assetType {
            case .Character:
                return "jpg"
            default:
                return "png"
            }
        }
    }
    
    init(assetType: AssetType, assetID: Int, assetWidth: Int) {
        self.assetType = assetType
        self.assetID = assetID
        self.assetWidth = assetWidth
        super.init()
    }
    
    func fetch() {
        if let imageFromCache = cachedImage() {
            self.image = imageFromCache
            delegate?.imageFetcherDidFinishSuccessfully(self, withImage: imageFromCache)
        }
        
        downloadData = NSMutableData()
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: self, delegateQueue: nil)
        
        let components = NSURLComponents()
        components.scheme = "https"
        components.host = "image.eveonline.com"
        components.path = "/\(assetType.rawValue)/\(filename)"
        if let theUrl = components.URL {
            url = theUrl
            let request = NSURLRequest(URL: theUrl)
            let task = session.dataTaskWithRequest(request)
            task.resume()
        }
        
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        downloadData.appendData(data)
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        if let image = UIImage(data: downloadData) {
            cacheImage(image)
            delegate?.imageFetcherDidFinishSuccessfully(self, withImage: image)
        } else {
            delegate?.imageFetcherDidFinishUnsuccessfully(self)
        }
        
    }
    
    func cacheImage(img: UIImage) {
        let now = NSDate()
        NSUserDefaults.standardUserDefaults().setValue(now, forKey: filename)
        let fileLocation = applicationSupportDirectory().URLByAppendingPathComponent(filename)
        let data = UIImagePNGRepresentation(img)
        data.writeToURL(fileLocation, atomically: true)
    }
    
    func cachedImage() -> UIImage? {
        //get the time when the cache was made
        if let cacheTime = NSUserDefaults.standardUserDefaults().valueForKey(filename) as? NSDate {
            let cachedUntil = cacheTime.dateByAddingTimeInterval(Double(cacheTimeInSeconds))
            if NSDate() < cachedUntil {
                let fileLocation = applicationSupportDirectory().URLByAppendingPathComponent(filename)
                if let data = NSData(contentsOfURL: fileLocation) {
                    if let cachedImage = UIImage(data: data) {
                        return cachedImage
                    }
                }
            }
        }
        
        return nil
    }
    
    func applicationSupportDirectory() -> NSURL {
        var error : NSError?
        let applicationSupport: NSURL?
        do {
            applicationSupport = try NSFileManager.defaultManager().URLForDirectory(NSSearchPathDirectory.ApplicationSupportDirectory, inDomain: NSSearchPathDomainMask.UserDomainMask, appropriateForURL: nil, create: true)
        } catch let error1 as NSError {
            error = error1
            applicationSupport = nil
        }
        let specificDirectory = applicationSupport!.URLByAppendingPathComponent("com.mmuszynski.EVEApiHelper", isDirectory: true)
        
        if !NSFileManager.defaultManager().fileExistsAtPath(specificDirectory.path!) {
            do {
                try NSFileManager.defaultManager().createDirectoryAtURL(specificDirectory, withIntermediateDirectories: true, attributes: nil)
            } catch let error1 as NSError {
                error = error1
            }
        }
        
        return specificDirectory
    }
    
    
}

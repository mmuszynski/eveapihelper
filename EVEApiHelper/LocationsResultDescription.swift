//
//  LocationsResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/28/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class LocationsResultDescription: ResultDescription {
    var locations : [LocationsRow] {
        get {
            if let rowset = rowSets["locations"] {
                return rowset.rows as! [LocationsRow]
            } else {
                return []
            }
        }
    }
}

//
//  StarbaseDetailViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/27/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class StarbaseAssetCell: UITableViewCell, ImageServerFetcherDelegate {
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemTypeLabel: UILabel!
    @IBOutlet weak var itemDetailLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var itemProgressView: UIProgressView!
    
    var imageFetcher: ImageServerFetcher?
    
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: UIImage) {
        spinner.stopAnimating()
        itemImageView.image = image
    }
    
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher) {
        
    }
    
}

class StarbaseDetailViewController: UITableViewController, GenericXMLFetcherDelegate {
    
    var starbase: Starbase!
    var nameFromTypeID: [Int64: String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = starbase.name

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Basic Information"
        case 1:
            return "Resources"
        default:
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return starbase.silos.count + 1
        default:
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("starbaseAssetCell", forIndexPath: indexPath) as! StarbaseAssetCell
            
            if indexPath.row == 0 {
                
                let typeID = starbase.fuelBlockType
                
                cell.itemNameLabel.text = "Fuel"
                cell.itemTypeLabel.text = nameFromTypeID[typeID]
                
                cell.itemProgressView.progress = Float(starbase.fuelBlockQuantity) / Float(28000)
                if starbase.fuelIsLow {
                    cell.itemProgressView.tintColor = UIColor.redColor()
                }
                
                let numberFormatter = NSNumberFormatter()
                numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                let quantity = numberFormatter.stringFromNumber(starbase.fuelBlockQuantity)!
                
                cell.itemDetailLabel.text = "\(quantity) / 28,000 blocks"
                cell.imageFetcher = ImageServerFetcher(assetType: .Render, assetID: Int(typeID), assetWidth: 128)
                cell.imageFetcher?.delegate = cell
                cell.imageFetcher?.fetch()
                
            } else {
                let silo = starbase.silos[indexPath.row-1]
                let name = silo.name
                
                cell.itemNameLabel.text = "Silo - \(name)"
                cell.itemTypeLabel.text = nameFromTypeID[silo.contentTypeID]
                
                let numberFormatter = NSNumberFormatter()
                numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
                let volume = numberFormatter.stringFromNumber(silo.contentVolume)!
                let capacity = numberFormatter.stringFromNumber(silo.capacity)!
                
                cell.itemDetailLabel.text = "\(volume) / \(capacity) m3"
                cell.itemProgressView.progress = Float(silo.fillFraction)
                cell.imageFetcher = ImageServerFetcher(assetType: .Render, assetID: Int(silo.contentTypeID), assetWidth: 128)
                cell.imageFetcher?.delegate = cell
                cell.imageFetcher?.fetch()
                
                if silo.quantityWarning {
                    cell.itemProgressView.tintColor = UIColor.redColor()
                }
                
            }

            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier("starbaseListCell", forIndexPath: indexPath) as! StarbaseListOverviewCell
            let typeID = starbase.typeID
            let location = starbase.moonID
            
            cell.starbaseNameLabel.text = nameFromTypeID[location] ?? "\(location)"
            cell.starbaseTypeLabel.text = nameFromTypeID[typeID]
            cell.starbaseLocationLabel.text = starbase.onlineSinceString
            cell.imageFetcher = ImageServerFetcher(assetType: .Render, assetID: Int(typeID), assetWidth: 128)
            cell.imageFetcher?.delegate = cell
            cell.imageFetcher?.fetch()
            
            return cell
        }

    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return self.tableView.bounds.width * 165.0 / 400.0
        default:
            return self.tableView.bounds.width * 200.0 / 600.0
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableView(tableView, estimatedHeightForRowAtIndexPath: indexPath)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {}
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        print(fetcher.result.description)
        
    }
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {}
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {}
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?) {}

}

//
//  StarbaseListResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/26/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class StarbaseListResultDescription: ResultDescription {
    
    var starbases : [StarbaseListRow] {
        get {
            if let rowset = rowSets["starbases"] {
                return rowset.rows as! [StarbaseListRow]
            } else {
                return []
            }
        }
    }
    
}

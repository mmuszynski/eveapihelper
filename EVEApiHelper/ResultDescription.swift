//
//  ResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/17/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

extension NSObject {
    
    func load(jsonInfo: NSDictionary) {
        for (key, value) in jsonInfo {
            var keyName = key as! String
            if keyName == "description" {
                keyName = "rawDesc"
            } else {
                let first = keyName.startIndex
                let restOfString = first.advancedBy(1)..<keyName.endIndex
                let capitalizedKey = keyName[first...first].uppercaseString + keyName[restOfString]
                
                keyName = "raw" + capitalizedKey
            }
            
            if (respondsToSelector(NSSelectorFromString(keyName))) {
                setValue(value, forKey: keyName)
            }
        }
    }
    
    func asJson() -> NSDictionary {
        let json = NSMutableDictionary()
        
        for name in propertyNames() {
            if let value: AnyObject = valueForKey(name) {
                json[name] = value
            }
        }
        
        
        return json
    }
    
    func propertyNames() -> [String] {
        var names: [String] = []
        var count: UInt32 = 0
        // Uses the Objc Runtime to get the property list

        var currentClass: AnyClass = classForCoder

         while currentClass.superclass() != nil {
            let properties = class_copyPropertyList(currentClass, &count)
            for var i = 0; i < Int(count); ++i {
                let property: objc_property_t = properties[i]
                let name: String =
                NSString(CString: property_getName(property), encoding: NSUTF8StringEncoding)! as String
                names.append(name)
            }
            free(properties)
            currentClass = currentClass.superclass()!
        }
        return names
    }
}

class ResultDescription: NSObject {
    
    var urlString = ""
    
    var rawCurrentTime = ""
    var rawCachedUntil = ""
    var rawApiVersion = ""
    var attributeDictionary = NSDictionary()
    var rowSets = [String : RowsetDescription]()
    var rowsetArray : [RowsetDescription] {
        get {
            return rowSets.values.array
        }
    }
    
    var currentTime : NSDate? {
        get {
            return NSDateFromEVEDateString(rawCurrentTime)
        }
    }
    var cachedUntil : NSDate? {
        get {
            return NSDateFromEVEDateString(rawCachedUntil)
        }
    }
    var apiVersion : Int? {
        get {
            return Int(rawApiVersion)
        }
    }
    var apiCall : EVEApiHelperCall?
    var cacheLength: Int = 1.hour
    var dataGenereationTime: NSDate? {
        return cachedUntil?.dateByAddingTimeInterval(NSTimeInterval(-cacheLength))
    }
    var dataAge: Int {
        if let grabTime = self.currentTime, let dataGenerationTime = self.dataGenereationTime {
            return Int(grabTime.timeIntervalSinceDate(dataGenerationTime))
        }
        
        return 0
    }
    
    init(attributeDict: NSDictionary) {
        super.init()
        self.attributeDictionary = attributeDict
        self.load(attributeDict)
    }
    
    override var description : String {
        get {
            var output = self.dynamicType.description()
            
            for name in self.propertyNames() {
                if name != "description" {
                    output += "\n>\(name): \(valueForKey(name)!)"
                }
            }
            return output
        }
    }
    
    func getStringForKey(key: String) -> String? {
        return attributeDictionary[key] as? String
    }
    
    func getIntForKey(key: String) -> Int? {
        let theInt = getStringForKey(key)
        return Int(theInt?)
    }
    
    func getDateForKey(key: String) -> NSDate? {
        if let dateString = getStringForKey(key) {
            return dateStringToNSDate(dateString)
        }
        
        return nil
    }
    
    func getInt64ForKey(key: String) -> Int64? {
        if let theInt = getStringForKey(key) {
            return NSNumberFormatter().numberFromString(theInt)?.longLongValue
        }
        return nil
    }

}

func dateStringToNSDate(string: String) -> NSDate? {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
    var dateString = string
    dateString += " GMT"
    return dateFormatter.dateFromString(dateString)
}

func NSDateFromEVEDateString(string: String) -> NSDate? {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
    var dateString = string
    dateString += " GMT"
    return dateFormatter.dateFromString(dateString)
}

func dateStringWithFormat(formatString: String, fromDate date: NSDate) -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = formatString
    return dateFormatter.stringFromDate(date)
}
//
//  AppDelegate.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/12/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSOutlineViewDataSource, NSOutlineViewDelegate, GenericXMLFetcherDelegate {

    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var detailView: NSView!
    @IBOutlet weak var viewController : NSViewController!
    
    var heightConstraInt : NSLayoutConstraInt?
    var widthConstraInt : NSLayoutConstraInt?
    
    var apiGroups = [OutlineItemGroup]()
    
    override init() {
        
        var apiCalls = [OutlineItem]()
        apiCalls.append(OutlineItem(name: "ApiCalls", controller: ApiCallsViewController(nibName: "ApiCallsViewController", bundle: NSBundle.mainBundle())!))
        let apiGroup = OutlineItemGroup(items: apiCalls, name: "API")
        apiGroups.append(apiGroup)

        var eveCalls = [OutlineItem]()
        eveCalls.append(OutlineItem(name: "AllianceList", controller: AlliancesViewController(nibName: "AlliancesViewController", bundle: NSBundle.mainBundle())!))
        let eveGroup = OutlineItemGroup(items: eveCalls, name: "EVE")
        apiGroups.append(eveGroup)
        
    }
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        
        // Insert code here to initialize your application
        //if let url = NSURL(string: "https://api.eveonline.com/eve/SkillTree.xml.aspx") {
        //if let url = NSURL(string: "https://api.eveonline.com/account/APIKeyInfo.xml.aspx"){
        //if let url = NSURL(string: "https://api.eveonline.com/Eve/AllianceList.xml.aspx") {

    }
    
    
    //provided for testing, but might be unused
    func XMLFetcherDidReportProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
    }
    func XMLFetcherDidFinish(fetcher: GenericXMLFetcher) {
        prIntln( fetcher.result )
    }
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?) {
        
        
    }
    
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        
        
    }
    
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        
    }
    
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {
        
        
    }
    
    
    func replaceContentViewWithViewFromController(controller: NSViewController) {
        
        if let subview = detailView.subviews.last as? NSView {
            detailView.replaceSubview(subview, with: controller.view)
        } else {
            detailView.addSubview(controller.view)
        }
        
        if let subview = detailView.subviews.last as? NSView {
            
            let views = [ "subview" : subview ] as NSDictionary
            detailView.addConstraInts(
                NSLayoutConstraInt.constraIntsWithVisualFormat("H:|[subview]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views as! [NSObject : AnyObject]) +
                NSLayoutConstraInt.constraIntsWithVisualFormat("V:|[subview]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views as! [NSObject : AnyObject]))
           
        }
        
    }
    
    func outlineViewSelectionDidChange(notification: NSNotification) {
        if let outlineView = notification.object as? NSOutlineView {
            if let item = outlineView.itemAtRow(outlineView.selectedRow) as? OutlineItem {
                replaceContentViewWithViewFromController(item.controller)
            }
        }
    }
    
    func outlineView(outlineView: NSOutlineView, numberOfChildrenOfItem item: AnyObject?) -> Int {
        if item == nil {
            return apiGroups.count
        } else {
            return 1;
        }
    }
    
    func outlineView(outlineView: NSOutlineView, child index: Int, ofItem item: AnyObject?) -> AnyObject {
        if item == nil {
            return apiGroups[index]
        } else if let theItem = item as? OutlineItemGroup {
            return theItem.items[index]
        }
        return ""
    }
    
    func outlineView(outlineView: NSOutlineView, isItemExpandable item: AnyObject) -> Bool {
        return item is OutlineItemGroup
    }
    
    func outlineView(outlineView: NSOutlineView, shouldSelectItem item: AnyObject) -> Bool {
        return !(item is OutlineItemGroup)
    }
    
    func outlineView(outlineView: NSOutlineView, objectValueForTableColumn tableColumn: NSTableColumn?, byItem item: AnyObject?) -> AnyObject? {
        if let theItem = item as? OutlineItemGroup {
            return theItem.groupName
        } else if let theItem = item as? OutlineItem {
            return theItem.name
        }
        return "Name Not Found"
    }
    
    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

class OutlineItemGroup : NSObject {
    var items : [OutlineItem]
    var groupName : String
    
    init(items: [OutlineItem], name: String) {
        self.items = items
        self.groupName = name
        super.init()
    }
}

class OutlineItem : NSObject {
    var name : String
    var controller : NSViewController
    
    init(name: String, controller: NSViewController) {
        self.name = name
        self.controller = controller
        super.init()
    }
}


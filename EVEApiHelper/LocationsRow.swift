//
//  LocationRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/28/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class LocationsRow: GenericRowDescription {
    
    var itemID: Int64? {
        get {
            return getInt64ForKey("itemID")
        }
    }
    
    var itemName: String? {
        get {
            return getStringForKey("itemName")
        }
    }
    
    var location: (x: Double?, y: Double?, z: Double?) {
        get {
            if let x = getDoubleForKey("x"), let y = getDoubleForKey("y"), let z = getDoubleForKey("z") {
                return (x: x, y: y, z: z)
            }
            
            return (nil, nil, nil)
        }
    }
    
    func squaredDistanceToLocation(otherLocation: LocationsRow) -> Double {
        let location1 = self.location
        let location2 = otherLocation.location
        
        let locDiffs = (location2.x! - location1.x!, location2.y! - location1.y!, location2.z! - location1.z!)
        
        let squared = locDiffs.0 * locDiffs.0 + locDiffs.1 * locDiffs.1 + locDiffs.2 * locDiffs.2
        return squared
    }
    
    func distanceToLocation(otherLocation: LocationsRow) -> Double {
        return sqrt(squaredDistanceToLocation(otherLocation))
    }
   
}

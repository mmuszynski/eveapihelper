//
//  Starbase.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 3/5/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class Starbase {
    
    // MARK: - Tower Type Data
    
    var smallTowers: [Int64] = [20060, 20062, 20064, 20066]
    var mediumTowers: [Int64] = [20059, 20061, 20063, 20065]
    var largeTowers: [Int64] = [12235, 12236, 16213, 16214]
    var small90PctTowers: [Int64] = [27592, 27598, 27604, 27610, 27784]
    var medium90PctTowers: [Int64] = [27589, 27595, 27601, 27607, 27782]
    var large90PctTowers: [Int64] = [27530, 27533, 27536, 27539, 27780]
    var small80PctTowers: [Int64] = [27594, 27600, 27606, 27612, 27790]
    var medium80PctTowers: [Int64] = [27591, 27597, 27603, 27609, 27788]
    var large80PctTowers: [Int64] = [27532, 27535, 27538, 27540, 27786]
    
    var fuelBlocksPerHour: Int {
        return towerFuelUsePerHour(self.typeID)
    }
    func towerFuelUsePerHour(typeID: Int64) -> Int {
        if smallTowers.indexOf(typeID) != nil {
            return 10
        } else if mediumTowers.indexOf(typeID) != nil {
            return 20
        } else if largeTowers.indexOf(typeID) != nil {
            return 40
        } else if small90PctTowers.indexOf(typeID) != nil {
            return 9
        } else if medium90PctTowers.indexOf(typeID) != nil {
            return 18
        } else if large90PctTowers.indexOf(typeID) != nil {
            return 36
        } else if small80PctTowers.indexOf(typeID) != nil {
            return 8
        } else if medium80PctTowers.indexOf(typeID) != nil {
            return 16
        } else if large80PctTowers.indexOf(typeID) != nil {
            return 32
        }
        
        return 0
    }
    
    // MARK: - Tower properties
    
    var starbaseAssets = [AssetListRow]()
    var starbaseAssetLocations = [LocationsRow]()
    var typeID: Int64
    var moonID: Int64
    var systemID: Int64
    var name: String
    var onlineSince: NSDate?
    var onlineSinceString: String
    var dataGrabTime: NSDate?
    var dataAge: Int
    var silos: [Silo]
    var fuelBlockType: Int64
    private var _fuelBlockQuantity: Int
    var fuelBlockQuantity: Int {
        return _fuelBlockQuantity - towerFuelUsePerHour(self.typeID) * _serverClicksSinceDataGrab
    }
    private var _strontiumQuantity: Int
    var fuelThreshold: Int {
        let float = NSUserDefaults.standardUserDefaults().floatForKey("com.mmuszynski.POSStatus.fuelThreshold")
        return Int(float).days

    }
    var fuelIsLow: Bool {
        return secondsFromHMS(timeUntilEmpty) < fuelThreshold
    }
    var silosCritical: Bool {
        return silos.map({ $0.quantityWarning }).reduce(false)  {
            (sum, next) in
            return sum || next
        }
    }
    var resourcesAreCritical: Bool {
        return fuelIsLow || silosCritical
    }
    
    // MARK: - Time functions
    
    private var _serverTick: (h: Int, m: Int, s: Int)? {
        if let onlineTime = self.onlineSince {
            return HMSFromDate(onlineTime)
        }
        
        return nil
    }
    private var _serverClicksSinceDataGrab: Int {
        var ticks = 0
        
        if let clickTime = _serverTick, dataTime = self.dataGrabTime {
            if let now = HMSFromDate(NSDate()), let dataGrabTime = HMSFromDate(dataTime) {
                ticks += dataGrabTime.h - now.h
                if (now.m.minute + now.s) > (clickTime.m.minute + clickTime.s) {
                    ticks++
                }
                if (now.h.hour + now.m.minute + now.s) - (dataGrabTime.h.hour + dataGrabTime.m.minute + dataGrabTime.s) < 1.hour {
                    ticks--
                }
            } else {
                print("could not get HMS times")
            }
        }
        
        return ticks
    }
    var hoursUntilFuelEmpty: Int {
        return Int(ceil(Float(fuelBlockQuantity) / Float(fuelBlocksPerHour)))
    }
    var timeUntilEmpty: (h: Int, m: Int, s: Int) {
        if var now = HMSFromDate(NSDate()), tickTime = _serverTick {
            now.h = 0
            tickTime.h = 0
            if secondsFromHMS(now) > secondsFromHMS(tickTime) {
                tickTime.h = 1
            }
            
            let secsUntilNextClick = secondsFromHMS(tickTime) - secondsFromHMS(now)
            var hms = HMSFromSeconds(secsUntilNextClick)
            hms.h = hoursUntilFuelEmpty - 1
            return hms
        }
        
        return (0, 0, 0)
    }
    func HMSFromSeconds(sec: Int) -> (h: Int, m: Int, s: Int) {
        let h = sec / 1.hour
        let m = (sec - h * 1.hour) / 1.minute
        let s = (sec - h * 1.hour - m * 1.minute)
        
        return (h, m, s)
    }
    func secondsFromHMS(hms: (h: Int, m: Int, s: Int)) -> Int {
        return hms.h.hours + hms.m.minutes + hms.s
    }
    func HMSFromDate(date: NSDate) -> (h: Int, m: Int, s: Int)? {
        let components = NSCalendar.currentCalendar().components([.Hour, .Minute, .Second], fromDate: date)
        return (h: components.hour, m: components.minute, s: components.second)
    }
    
    // MARK: - Initalizers
    
    init(starbaseListRow: StarbaseListRow, starbaseDetailResult: StarbaseDetailResultDescription, locationsResult: LocationsResultDescription, assetListResult: AssetListResultDescription, typeIDConversions: [Int64 : String]?) {
        let location = starbaseListRow.locationID
        let towerID = starbaseListRow.itemID
        let itemsInLocation = assetListResult.assetArray.filter { $0.locationID == location }.map { $0.itemID! }
        
        //this is a list of locations that are in the same solar system as the tower
        let solarSystemItems = locationsResult.locations.filter( { itemsInLocation.indexOf($0.itemID!) != nil && $0.itemID != towerID })
        if let towerLocation = locationsResult.locations.filter( { $0.itemID == towerID } ).first {
            //get only the ones that are a certain distance from the tower, in this case, 200km is probably okay
            let towerItemIDs = solarSystemItems.filter( { towerLocation.distanceToLocation($0) < 200000 } ).map { $0.itemID! }
            starbaseAssets = assetListResult.assetArray.filter { towerItemIDs.indexOf($0.itemID!) != nil }
            starbaseAssetLocations = locationsResult.locations.filter { towerItemIDs.indexOf($0.itemID!) != nil }
            
            self.name = towerLocation.itemName!
            
        } else {
            fatalError("could not get tower location for some reason")
        }
        
        self.typeID = starbaseListRow.typeID!
        self.moonID = starbaseListRow.moonID!
        self.systemID = starbaseListRow.locationID!
        self.onlineSince = starbaseListRow.onlineTimestamp
        self.onlineSinceString = starbaseListRow.onlineSinceString
        if let stront = starbaseDetailResult.fuelRows.filter( { $0.typeID! == 16275 } ).first?.quantity {
            self._strontiumQuantity = stront
        } else {
            self._strontiumQuantity = 0
        }
        if let fuel = starbaseDetailResult.fuelRows.filter( { $0.typeID! != 16275 } ).first {
            self._fuelBlockQuantity = fuel.quantity!
            self.fuelBlockType = fuel.typeID!
        } else {
            self._fuelBlockQuantity = 0
            self.fuelBlockType = 0
        }
        
        dataGrabTime = starbaseDetailResult.dataGenereationTime
        dataAge = starbaseDetailResult.dataAge
        
        self.silos = [Silo]()
        
        let siloRows = starbaseAssets.filter({ $0.typeID == 14343 })
        for assetRow in siloRows {
            let id = assetRow.itemID!
            let locationRow = starbaseAssetLocations.filter { $0.itemID == id }.first!
            silos.append( Silo(assetRow: assetRow, name: locationRow.itemName!) )
        }
        
    }
    
}

class Silo {
    var contentSizes: [Int64:Double] = [0 : 0, 16656 : 1.0, 16660 : 1.0, 16673 : 0.01]

    var name: String
    var contentTypeID: Int64 = 0
    var contentQuantity: Int = 0
    var capacity = 20000.0
    var contentVolume: Double {
        let quantity = Double(contentQuantity)
        let unitVolume = contentSizes[contentTypeID] ?? 0
        return quantity * unitVolume
    }
    var fillFraction: Double {
        return Double(contentVolume) / Double(capacity)
    }
    var emptinessThreshold: Float {
        return NSUserDefaults.standardUserDefaults().floatForKey("com.mmuszynski.POSStatus.inputThreshold") ?? 0.0
    }
    var fullnessThreshold: Float {
        return NSUserDefaults.standardUserDefaults().floatForKey("com.mmuszynski.POSStatus.outputThreshold") ?? 0.0
    }
    var containsInputProducts: Bool {
        return contentTypeID == 16656 || contentTypeID == 16660
    }
    var quantityWarning: Bool {
        if self.containsInputProducts {
            return contentVolume <= capacity * Double(emptinessThreshold)
        } else {
            return contentVolume >= capacity * Double(fullnessThreshold)
        }
    }
    
    init(assetRow: AssetListRow, name: String) {
        if let siloContents = assetRow.contents {
            contentTypeID = siloContents.first?.typeID ?? 0
            contentQuantity = siloContents.first?.quantity ?? 0
        }
        
        self.name = name
    }
    
}
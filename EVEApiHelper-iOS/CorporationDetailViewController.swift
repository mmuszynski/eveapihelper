//
//  CorporationDetailViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/28/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class CorporationDetailViewController: UITableViewController, ImageServerFetcherDelegate {
    var corporation : CorporationRow!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tickerLabel: UILabel!
    @IBOutlet weak var membersLabel: UILabel!
    @IBOutlet weak var stationLabel: UILabel!
    @IBOutlet weak var ceoLabel: UILabel!
    @IBOutlet weak var taxRateLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var corpImageView: UIImageView!
    @IBOutlet weak var ceoImageView: UIImageView!
    
    @IBOutlet weak var ceoImageSpinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = corporation.corporationName
        tickerLabel.text = "[\(corporation.ticker)]"
        membersLabel.text = "\(corporation.memberCount!) " + simplePluralize("member", corporation.memberCount!)
        stationLabel.text = "HQ: " + corporation.stationName
        ceoLabel.text = corporation.ceoName
        taxRateLabel.text = "Tax Rate: " + corporation.taxRate + "%"
        
        if let corpURLString = corporation.url {
            if let corpURL = NSURL(string: corpURLString) {
                let attrString = NSMutableAttributedString(string: corpURLString)
                attrString.addAttribute(NSLinkAttributeName, value:corpURL, range: NSRangeFromString(attrString.string))
                urlLabel.attributedText = attrString
            }
        }
        
        descriptionLabel.text = corporation.desc
        
        let fetcher = ImageServerFetcher(assetType: .Corporation, assetID: corporation.corporationID!, assetWidth: 128)
        fetcher.delegate = self
        fetcher.fetch()
        let ceoFetcher = ImageServerFetcher(assetType: .Character, assetID: corporation.ceoID!, assetWidth: 128)
        ceoFetcher.delegate = self
        ceoFetcher.fetch()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: UIImage) {
        if fetcher.assetType == .Corporation {
            corpImageView.image = image
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                self.ceoImageView.image = image
                self.ceoImageSpinner.stopAnimating()
            })
        }
    }
    
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher) {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

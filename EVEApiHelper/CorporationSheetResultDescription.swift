//
//  CorporationSheetResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/27/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
import Cocoa
#elseif os(iOS)
import UIKit
#endif

class CorporationSheetResultDescription: ResultDescription {
    
    var corporationRow : CorporationRow {
        get {
            return CorporationRow(attributeDict: self.attributeDictionary)
        }
    }
    
}

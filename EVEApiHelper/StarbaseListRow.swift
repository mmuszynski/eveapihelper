//
//  StarbaseListRowDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/26/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class StarbaseListRow: GenericRowDescription {
    
    var itemID: Int64?  {
        get {
            return self.getInt64ForKey("itemID")
        }
    }
    var locationID: Int64? {
        get {
            return self.getInt64ForKey("locationID")
        }
    }
    var moonID: Int64? {
        get {
            return self.getInt64ForKey("moonID")
        }
    }
    var onlineTimestamp: NSDate? {
        get {
            return self.getDateForKey("onlineTimestamp")
        }
    }
    var standingOwnerID: Int? {
        get {
            return self.getIntForKey("standingOwnerID")
        }
    }
    var state: Int? {
        get {
            return self.getIntForKey("state")
        }
    }
    var stateTimestamp: NSDate? {
        get {
            return self.getDateForKey("stateTimestamp")
        }
    }
    var typeID: Int64? {
        get {
            return self.getInt64ForKey("typeID")
        }
    }
    
    var onlineSinceString: String {
        let formatter = NSDateFormatter()
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
        formatter.timeStyle = NSDateFormatterStyle.NoStyle
        formatter.locale = NSLocale.currentLocale()
        return "Online Since: \(formatter.stringFromDate(onlineTimestamp!))"
    }

}

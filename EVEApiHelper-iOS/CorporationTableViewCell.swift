//
//  CorporationTableViewCell.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/26/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class CorporationTableViewCell: UITableViewCell {

    @IBOutlet weak var corpImageView: UIImageView!
    @IBOutlet weak var corpNameLabel: UILabel!
    @IBOutlet weak var corpNumbersLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

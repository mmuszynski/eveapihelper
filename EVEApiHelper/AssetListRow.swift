//
//  AssetListRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/27/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class AssetListRow: GenericRowDescription {
    
    var contents: [AssetListRow]? {
        return self.childRowset?.rows as? [AssetListRow]
    }
    
    var typeID: Int64? {
        return getInt64ForKey("typeID")
    }
    
    var itemID: Int64? {
        return getInt64ForKey("itemID")
    }
    
    var flag: InventoryLocationFlag {
        let theFlag = getIntForKey("flag")
        return InventoryLocationFlag(rawValue: theFlag!)!
    }
    
    var locationID: Int64? {
        return getInt64ForKey("locationID")
    }
    
    var quantity: Int? {
        return getIntForKey("quantity")
    }
    
    var singleton: Bool {
        return getIntForKey("singleton") == 1
    }
    
    var rawQuantity: Int? {
        return getIntForKey("rawQuantity")
    }
    
    var allNestedContents: [AssetListRow] {
        return getAllChildrenOfRow(self)
    }
    
    func getAllChildrenOfRow(row: AssetListRow) -> [AssetListRow] {
        var rows = [row]
        if let children = row.contents {
            for assetRow in children {
                rows += getAllChildrenOfRow(assetRow)
            }
        }
        
        return rows
    }
   
}

enum InventoryLocationFlag: Int {
    case None = 0,
    Wallet = 1,
    Factory = 2,
    Wardrobe = 3,
    Hangar = 4,
    Cargo = 5,
    Briefcase = 6,
    Skill = 7,
    Reward = 8,
    Connected = 9,
    Disconnected = 10,
    LoSlot0 = 11,
    LoSlot1 = 12,
    LoSlot2 = 13,
    LoSlot3 = 14,
    LoSlot4 = 15,
    LoSlot5 = 16,
    LoSlot6 = 17,
    LoSlot7 = 18,
    MedSlot0 = 19,
    MedSlot1 = 20,
    MedSlot2 = 21,
    MedSlot3 = 22,
    MedSlot4 = 23,
    MedSlot5 = 24,
    MedSlot6 = 25,
    MedSlot7 = 26,
    HiSlot0 = 27,
    HiSlot1 = 28,
    HiSlot2 = 29,
    HiSlot3 = 30,
    HiSlot4 = 31,
    HiSlot5 = 32,
    HiSlot6 = 33,
    HiSlot7 = 34,
    FixedSlot = 35,
    PromenadeSlot1 = 40,
    PromenadeSlot2 = 41,
    PromenadeSlot3 = 42,
    PromenadeSlot4 = 43,
    PromenadeSlot5 = 44,
    PromenadeSlot6 = 45,
    PromenadeSlot7 = 46,
    PromenadeSlot8 = 47,
    PromenadeSlot9 = 48,
    PromenadeSlot10 = 49,
    PromenadeSlot11 = 50,
    PromenadeSlot12 = 51,
    PromenadeSlot13 = 52,
    PromenadeSlot14 = 53,
    PromenadeSlot15 = 54,
    PromenadeSlot16 = 55,
    Capsule = 56,
    Pilot = 57,
    Passenger = 58,
    BoardingGate = 59,
    Crew = 60,
    SkillIntraining = 61,
    CorpMarket = 62,
    Locked = 63,
    Unlocked = 64,
    OfficeSlot1 = 70,
    OfficeSlot2 = 71,
    OfficeSlot3 = 72,
    OfficeSlot4 = 73,
    OfficeSlot5 = 74,
    OfficeSlot6 = 75,
    OfficeSlot7 = 76,
    OfficeSlot8 = 77,
    OfficeSlot9 = 78,
    OfficeSlot10 = 79,
    OfficeSlot11 = 80,
    OfficeSlot12 = 81,
    OfficeSlot13 = 82,
    OfficeSlot14 = 83,
    OfficeSlot15 = 84,
    OfficeSlot16 = 85,
    Bonus = 86,
    DroneBay = 87,
    Booster = 88,
    Implant = 89,
    ShipHangar = 90,
    ShipOffline = 91,
    RigSlot0 = 92,
    RigSlot1 = 93,
    RigSlot2 = 94,
    RigSlot3 = 95,
    RigSlot4 = 96,
    RigSlot5 = 97,
    RigSlot6 = 98,
    RigSlot7 = 99,
    FactoryOperation = 100,
    CorpSAG2 = 116,
    CorpSAG3 = 117,
    CorpSAG4 = 118,
    CorpSAG5 = 119,
    CorpSAG6 = 120,
    CorpSAG7 = 121,
    SecondaryStorage = 122,
    CaptainsQuarters = 123,
    WisPromenade = 124,
    SubSystem0 = 125,
    SubSystem1 = 126,
    SubSystem2 = 127,
    SubSystem3 = 128,
    SubSystem4 = 129,
    SubSystem5 = 130,
    SubSystem6 = 131,
    SubSystem7 = 132,
    SpecializedFuelBay = 133,
    SpecializedOreHold = 134,
    SpecializedGasHold = 135,
    SpecializedMineralHold = 136,
    SpecializedSalvageHold = 137,
    SpecializedShipHold = 138,
    SpecializedSmallShipHold = 139,
    SpecializedMediumShipHold = 140,
    SpecializedLargeShipHold = 141,
    SpecializedIndustrialShipHold = 142,
    SpecializedAmmoHold = 143,
    StructureActive = 144,
    StructureInactive = 145,
    JunkyardReprocessed = 146,
    JunkyardTrashed = 147,
    SpecializedCommandCenterHold = 148,
    SpecializedPlanetaryCommoditiesHold = 149,
    PlanetSurface = 150,
    SpecializedMaterialBay = 151,
    DustCharacterDatabank = 152,
    DustCharacterBattle = 153,
    QuafeBay = 154,
    FleetHangar = 155
    
    var description: String {
        get {
            switch self.rawValue {
            case 0: return "None"
            case 1: return "Wallet"
            case 2: return "Factory"
            case 3: return "Wardrobe"
            case 4: return "Hangar"
            case 5: return "Cargo"
            case 6: return "Briefcase"
            case 7: return "Skill"
            case 8: return "Reward"
            case 9: return "Character in station connected"
            case 10: return "Character in station offline"
            case 11: return "Low power slot 1"
            case 12: return "Low power slot 2"
            case 13: return "Low power slot 3"
            case 14: return "Low power slot 4"
            case 15: return "Low power slot 5"
            case 16: return "Low power slot 6"
            case 17: return "Low power slot 7"
            case 18: return "Low power slot 8"
            case 19: return "Medium power slot 1"
            case 20: return "Medium power slot 2"
            case 21: return "Medium power slot 3"
            case 22: return "Medium power slot 4"
            case 23: return "Medium power slot 5"
            case 24: return "Medium power slot 6"
            case 25: return "Medium power slot 7"
            case 26: return "Medium power slot 8"
            case 27: return "High power slot 1"
            case 28: return "High power slot 2"
            case 29: return "High power slot 3"
            case 30: return "High power slot 4"
            case 31: return "High power slot 5"
            case 32: return "High power slot 6"
            case 33: return "High power slot 7"
            case 34: return "High power slot 8"
            case 35: return "Fixed Slot"
            case 40: return "Promenade Slot 1"
            case 41: return "Promenade Slot 2"
            case 42: return "Promenade Slot 3"
            case 43: return "Promenade Slot 4"
            case 44: return "Promenade Slot 5"
            case 45: return "Promenade Slot 6"
            case 46: return "Promenade Slot 7"
            case 47: return "Promenade Slot 8"
            case 48: return "Promenade Slot 9"
            case 49: return "Promenade Slot 10"
            case 50: return "Promenade Slot 11"
            case 51: return "Promenade Slot 12"
            case 52: return "Promenade Slot 13"
            case 53: return "Promenade Slot 14"
            case 54: return "Promenade Slot 15"
            case 55: return "Promenade Slot 16"
            case 56: return "Capsule"
            case 57: return "Pilot"
            case 58: return "Passenger"
            case 59: return "Boarding gate"
            case 60: return "Crew"
            case 61: return "Skill in training"
            case 62: return "Corporation Market Deliveries / Returns"
            case 63: return "Locked item, can not be moved unless unlocked"
            case 64: return "Unlocked item, can be moved"
            case 70: return "Office slot 1"
            case 71: return "Office slot 2"
            case 72: return "Office slot 3"
            case 73: return "Office slot 4"
            case 74: return "Office slot 5"
            case 75: return "Office slot 6"
            case 76: return "Office slot 7"
            case 77: return "Office slot 8"
            case 78: return "Office slot 9"
            case 79: return "Office slot 10"
            case 80: return "Office slot 11"
            case 81: return "Office slot 12"
            case 82: return "Office slot 13"
            case 83: return "Office slot 14"
            case 84: return "Office slot 15"
            case 85: return "Office slot 16"
            case 86: return "Bonus"
            case 87: return "Drone Bay"
            case 88: return "Booster"
            case 89: return "Implant"
            case 90: return "Ship Hangar"
            case 91: return "Ship Offline"
            case 92: return "Rig power slot 1"
            case 93: return "Rig power slot 2"
            case 94: return "Rig power slot 3"
            case 95: return "Rig power slot 4"
            case 96: return "Rig power slot 5"
            case 97: return "Rig power slot 6"
            case 98: return "Rig power slot 7"
            case 99: return "Rig power slot 8"
            case 100: return "Factory Background Operation"
            case 116: return "Corp Security Access Group 2"
            case 117: return "Corp Security Access Group 3"
            case 118: return "Corp Security Access Group 4"
            case 119: return "Corp Security Access Group 5"
            case 120: return "Corp Security Access Group 6"
            case 121: return "Corp Security Access Group 7"
            case 122: return "Secondary Storage"
            case 123: return "Captains Quarters"
            case 124: return "Wis Promenade"
            case 125: return "Sub system slot 0"
            case 126: return "Sub system slot 1"
            case 127: return "Sub system slot 2"
            case 128: return "Sub system slot 3"
            case 129: return "Sub system slot 4"
            case 130: return "Sub system slot 5"
            case 131: return "Sub system slot 6"
            case 132: return "Sub system slot 7"
            case 133: return "Specialized Fuel Bay"
            case 134: return "Specialized Ore Hold"
            case 135: return "Specialized Gas Hold"
            case 136: return "Specialized Mineral Hold"
            case 137: return "Specialized Salvage Hold"
            case 138: return "Specialized Ship Hold"
            case 139: return "Specialized Small Ship Hold"
            case 140: return "Specialized Medium Ship Hold"
            case 141: return "Specialized Large Ship Hold"
            case 142: return "Specialized Industrial Ship Hold"
            case 143: return "Specialized Ammo Hold"
            case 144: return "StructureActive"
            case 145: return "StructureInactive"
            case 146: return "This item was put Into a junkyard through reprocession."
            case 147: return "This item was put Into a junkyard through being trashed by its owner."
            case 148: return "Specialized Command Center Hold"
            case 149: return "Specialized Planetary Commodities Hold"
            case 150: return "Planet Surface"
            case 151: return "Specialized Material Bay"
            case 152: return "Dust Character Databank"
            case 153: return "Dust Character Battle"
            case 154: return "Quafe Bay"
            case 155: return "Fleet Hangar"
            default: return "unknown"
            }
        }
    }
}

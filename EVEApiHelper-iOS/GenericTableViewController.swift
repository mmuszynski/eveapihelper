//
//  GenericTableViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/3/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class GenericTableViewController: UITableViewController, GenericXMLFetcherDelegate {
    
    var items = [GenericRowDescription]()
    var fetcher : GenericXMLFetcher?
    //@IBOutlet var progressView: UIProgressView!
    var progressView: UIProgressView!
    var backgroundButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetcher?.delegate = self
        
        let button = UIButton(frame: tableView.bounds)
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        button.setTitle("Tap anywhere to start", forState: .Normal)
        button.setTitle("Starting...", forState: .Disabled)
        button.addTarget(self, action: "startFetcher:", forControlEvents: .TouchUpInside)
        self.tableView.backgroundView = button
        backgroundButton = button
        
        progressView = UIProgressView(progressViewStyle: .Bar)
        self.tableView.tableFooterView = progressView
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func startFetcher(sender: AnyObject?) {
        fetcher?.start()
        if let button = sender as? UIButton {
            button.enabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsIntableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return items.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell
        let item = items[indexPath.row]
        cell.textLabel?.text = (item.rawAttributes["name"] as! String)

        // Configure the cell...

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it Into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {
        dispatch_async(dispatch_get_main_queue(), {
            self.navigationController?.topViewController.title = "Downloading..."
            self.progressView.setProgress(0, animated: true)
        })
    }
    
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        dispatch_async(dispatch_get_main_queue(), {
            if let theTotal = total {
                let progress = Float(num) / Float(theTotal)
                self.progressView.progress = progress
            } else {
                self.progressView.setProgress(0.95, animated: true)
            }
        })
    }
    
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError: NSError?) {
        dispatch_async(dispatch_get_main_queue(), {
            self.progressView.setProgress(1.0, animated: false)
        })
    }
    
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        if let result = fetcher.result {
            if let rowset = result.rowsetArray.first {
                items = rowset.rows
            }
        }
        
        
        dispatch_async(dispatch_get_main_queue(), {
            self.navigationController?.topViewController.title = ""
            self.progressView.setProgress(1.0, animated: true)
            self.progressView.hidden = true
            self.tableView.reloadData()
            self.tableView.scrollEnabled = true
            self.backgroundButton.setTitle("No Rows", forState: .Disabled)
        })
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        dispatch_async(dispatch_get_main_queue(), {
            self.navigationController?.topViewController.title = "Parsing..."
            
            if let theTotal = total {
                let progress = Float(num) / Float(theTotal)
                self.progressView.progress = progress
            }
        })
        
    }

}

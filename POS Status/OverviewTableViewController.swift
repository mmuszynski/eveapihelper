//
//  OverviewTableViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/26/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

@IBDesignable class StatusNibView: UIView {
    
    @IBInspectable var color: UIColor = UIColor.redColor() {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override func drawRect(rect: CGRect) {
        
        var hue: CGFloat = 0, sat: CGFloat = 0, bright: CGFloat = 0, alpha: CGFloat = 0
        self.color.getHue(&hue, saturation: &sat, brightness: &bright, alpha: &alpha)
        bright = max(bright * 0.75, 0)
        alpha = max(alpha * 0.75, 0)
        let drawColor = UIColor(hue: hue, saturation: sat, brightness: bright, alpha: alpha)
                
        let circleFrame = CGRectInset(self.bounds, 2.0, 2.0)
        let path = UIBezierPath(ovalInRect: circleFrame)
        drawColor.setFill()
        path.fill()
        
        UIColor.lightGrayColor().setStroke()
        path.lineWidth = 0.5
        path.stroke()
        
    }
    
}

class StarbaseListOverviewCell: UITableViewCell, ImageServerFetcherDelegate {
    
    @IBOutlet weak var tileView: UIView!
    @IBOutlet weak var starbaseImage: UIImageView!
    @IBOutlet weak var starbaseNameLabel: UILabel!
    @IBOutlet weak var starbaseTypeLabel: UILabel!
    @IBOutlet weak var starbaseLocationLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var starbaseFuelAmountLabel: UILabel!
    @IBOutlet weak var starbaseFuelStatusNib: StatusNibView!
    @IBOutlet weak var starbaseSiloStatusNib: StatusNibView!
    
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    var starbase: Starbase!
    
    var imageFetcher: ImageServerFetcher?
    
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: UIImage) {
        spinner.stopAnimating()
        starbaseImage.image = image
    }
    
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher) {
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
        self.tileView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        self.tileView.layer.shadowRadius = 1.5
        self.tileView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.tileView.layer.shadowOpacity = 1.0
    }
    
}

class OverviewTableViewController: UITableViewController, GenericXMLFetcherDelegate {
    
    let pleaseWait = UIAlertController(title: "Please Wait", message: "Downloading POS Information", preferredStyle: .Alert)
    let waitProgress = UIProgressView(progressViewStyle: .Bar)
    
    var starbasesBySystem = [Int64: [Starbase]]()
    var nameFromTypeID = [Int64: String]()
    
    var starbaseListResult: StarbaseListResultDescription?
    var starbaseDetailResults = [StarbaseDetailResultDescription]()
    var locationsResult: LocationsResultDescription?
    var assetListResult: AssetListResultDescription?
    
    var key: ApiKey!

    var assetList = [AssetListRow]()
    
    var typeIDs = Set<Int64>()
    var charIDs = Set<Int64>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        let view = UIImageView(image: UIImage(named: "bg"))
        view.contentMode = UIViewContentMode.TopLeft
        self.tableView.backgroundView = view
        //self.tableView.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    @IBAction func refreshPosInformation(sender: AnyObject) {
        pleaseWait.view.clipsToBounds = true
        pleaseWait.message = "Downloading Corp Asset List"
        pleaseWait.view.addSubview(waitProgress)
        
        waitProgress.progress = 0
        
        self.presentViewController(pleaseWait, animated: true, completion: nil)
        waitProgress.frame = pleaseWait.view.bounds
        starbasesBySystem.removeAll(keepCapacity: false)
        starbaseDetailResults.removeAll(keepCapacity: false)
        nameFromTypeID.removeAll(keepCapacity: false)
        
        key = ApiKey(keyId: 1823650, vCode: "fuGF9st2fo6LDyvnEuF3e2JN6DoYlFtKu3mEV9Cr39poFWldBYzPodK7idcASSF4")
        
        let corpAssetCall = EVEApiHelperCall(callGroup: .Corporation, callType: .AssetList)
        let corpAssetFetcher = try? GenericXMLFetcher(apiCall: corpAssetCall)
        corpAssetFetcher?.apiKey = key
        corpAssetFetcher?.delegate = self
        corpAssetFetcher?.start()
        
        charIDs = Set<Int64>()
        typeIDs = Set<Int64>()
        
    }
    
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {
        dispatch_async(dispatch_get_main_queue(), {
            switch fetcher.apiCall!.callType {
            case .AssetList:
                self.pleaseWait.message = "Downloading Corp Asset List"
                self.waitProgress.setProgress(0.1, animated: true)
            case .StarbaseList:
                self.pleaseWait.message = "Downloading Starbase List"
                self.waitProgress.setProgress(0.35, animated: true)
            case .TypeName, .CharacterName:
                self.pleaseWait.message = "Downloading Item Name Information"
                self.waitProgress.setProgress(0.85, animated: true)
            case .StarbaseDetail:
                self.pleaseWait.message = "Downloading Starbase Details"
                self.waitProgress.setProgress(0.7, animated: true)
            default:
                self.pleaseWait.message = "Downloading POS List"
                self.waitProgress.setProgress(0.15, animated: true)
            }
            
        })
    }
    
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?) {
    }
    
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
    }

    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        var completionHandler: (() -> ())? = nil
        
        if let result = fetcher.result as? ErrorResultDescription {
            completionHandler = {
                let errorAlert = UIAlertController(title: "API Error \(result.errorCode!)", message: result.error, preferredStyle: .Alert)
                errorAlert.addAction(UIAlertAction(title: "Okay", style: .Cancel, handler: nil))
                self.presentViewController(errorAlert, animated: true, completion: nil)
            }
        } else if let assetList = fetcher.result as? AssetListResultDescription {
            
            self.assetListResult = assetList
            
            let allAssets = assetList.assetArray.map {
                $0.allNestedContents
            }.reduce([], combine: +).map{$0.typeID!}
            
            let assetIDSet = Set(allAssets)
            
            typeIDs.unionInPlace(assetIDSet)
            
            //get locations for things in space
            let itemsInSpace = (assetList.assetArray.filter { $0.flag == InventoryLocationFlag.None }).map { $0.itemID! }
            let call = EVEApiHelperCall(callGroup: .Corporation, callType: .Locations)
            let locationFetcher = try? GenericXMLFetcher(apiCall: call)
            locationFetcher?.useCache = false
            locationFetcher?.delegate = self
            locationFetcher?.apiKey = key
            locationFetcher?.idList = itemsInSpace
            locationFetcher?.start()
            
        } else if let locationsResult = fetcher.result as? LocationsResultDescription {
            
            self.locationsResult = locationsResult
            
            let call = EVEApiHelperCall(callGroup: .Corporation, callType: .StarbaseList)
            let starbaseListFetcher = try? GenericXMLFetcher(apiCall: call)
            starbaseListFetcher?.useCache = false
            starbaseListFetcher?.delegate = self
            starbaseListFetcher?.apiKey = key
            starbaseListFetcher?.start()
    
        } else if let result = fetcher.result as? StarbaseListResultDescription {
            
            self.starbaseListResult = result

            for row in result.starbases {
                typeIDs.insert(row.typeID!)
                
                charIDs.insert(row.moonID!)
                charIDs.insert(row.locationID!)
                
                let starbaseDetailCall = EVEApiHelperCall(callGroup: .Corporation, callType: .StarbaseDetail)
                let detailFetcher = try? GenericXMLFetcher(apiCall: starbaseDetailCall)
                detailFetcher?.itemID = row.itemID
                detailFetcher?.delegate = self
                detailFetcher?.useCache = false
                detailFetcher?.apiKey = key
                detailFetcher?.start()
                
            }
            
        } else if let nameRows = fetcher.result.rowsetArray.first?.rows as? [GenericNameRow] {
            for row in nameRows {
                nameFromTypeID[row.id!] = row.name
            }
            
            if fetcher.apiCall!.callType == .TypeName {
                let characterIDCall = EVEApiHelperCall(callGroup: .EVE, callType: .CharacterName)
                let characterIDFetcher = try? GenericXMLFetcher(apiCall: characterIDCall)
                characterIDFetcher?.useCache = false
                characterIDFetcher?.delegate = self
                characterIDFetcher?.idList = Array(charIDs)
                characterIDFetcher?.start()
            } else {
                completionHandler = {
                    self.parseTowers()
                    self.tableView.reloadData()
                }
            }
            
        } else if let posDetail = fetcher.result as? StarbaseDetailResultDescription {
            starbaseDetailResults.append( posDetail )
            let fuelIDs = posDetail.fuelRows.map( { $0.typeID! } )
            typeIDs.unionInPlace( Set(fuelIDs) )
            
            if starbaseDetailResults.count == starbaseListResult?.starbases.count {
                
                let corpLocsCall = EVEApiHelperCall(callGroup: .Corporation, callType: .Locations)
                let corpLocsFetcher = try? GenericXMLFetcher(apiCall: corpLocsCall)
                corpLocsFetcher?.delegate = self
                corpLocsFetcher?.useCache = false
                
                let typeIDCall = EVEApiHelperCall(callGroup: .EVE, callType: .TypeName)
                let typeIDFetcher = try? GenericXMLFetcher(apiCall: typeIDCall)
                typeIDFetcher?.useCache = false
                typeIDFetcher?.delegate = self
                typeIDFetcher?.idList = Array(typeIDs)
                typeIDFetcher?.start()
                
            }

        }
        
        if completionHandler != nil {
            pleaseWait.dismissViewControllerAnimated(true, completion: completionHandler)
        }
    }
    
    func parseTowers() {
        for tower in self.starbaseListResult!.starbases {
            if let towerDetail = self.starbaseDetailResults.filter( { $0.itemID == tower.itemID }).first {
                let starbase = Starbase(starbaseListRow: tower, starbaseDetailResult: towerDetail, locationsResult: locationsResult!, assetListResult: assetListResult!, typeIDConversions: nameFromTypeID)
                let system = starbase.systemID
                
                if starbasesBySystem[system] == nil {
                    starbasesBySystem[system] = [Starbase]()
                }

                starbasesBySystem[system]?.append(starbase)
                
            } else {
               fatalError("could not find tower detail")
            }
        }
    
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return starbasesBySystem.keys.array.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let system = starbasesBySystem.keys.array[section]
        return nameFromTypeID[system] ?? "\(system)"
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        let system = starbasesBySystem.keys.array[section]
        return starbasesBySystem[system]?.count ?? 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("starbaseListCell", forIndexPath: indexPath) as! StarbaseListOverviewCell
        let system = starbasesBySystem.keys.array[indexPath.section]
        if let tower = starbasesBySystem[system]?[indexPath.row] {
            cell.starbase = tower
            
            cell.starbaseNameLabel.text = tower.name
            cell.starbaseLocationLabel.text = nameFromTypeID[tower.moonID] ?? "\(tower.moonID)"
            cell.starbaseTypeLabel.text = nameFromTypeID[tower.typeID]
            cell.imageFetcher = ImageServerFetcher(assetType: .Render, assetID: Int(tower.typeID), assetWidth: 256)
            cell.imageFetcher?.delegate = cell
            cell.imageFetcher?.fetch()
            
            if tower.fuelIsLow {
                cell.starbaseFuelStatusNib.color = UIColor.redColor()
            } else {
                cell.starbaseFuelStatusNib.color = UIColor.greenColor()
            }
            
            if tower.silosCritical {
                cell.starbaseSiloStatusNib.color = UIColor.redColor()
            } else {
                cell.starbaseSiloStatusNib.color = UIColor.greenColor()
            }
            
            
        }

        return cell
    }
    
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableView.bounds.width * 165.0 / 400.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableView(tableView, estimatedHeightForRowAtIndexPath: indexPath)
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UILabel()
        view.backgroundColor = UIColor(white: 1.0, alpha: 0.2)
        let system = starbasesBySystem.keys.array[section]
        let name = nameFromTypeID[system] ?? "\(system)"
        view.text = name
        view.textAlignment = .Center
        view.textColor = UIColor.whiteColor()
        return view
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if let indexPath = self.tableView.indexPathForSelectedRow, let cell = self.tableView.cellForRowAtIndexPath(indexPath) as? StarbaseListOverviewCell {
            let starbase = cell.starbase
            let vc = segue.destinationViewController as? StarbaseDetailViewController
            vc?.starbase = starbase
            vc?.nameFromTypeID = self.nameFromTypeID
        }
    }

}

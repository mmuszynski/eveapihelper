//
//  AllianceRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/18/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class AllianceRow: GenericRowDescription {
    
    var name : String {
        get {
            return self.rawAttributes["name"] as! String
        }
    }
    
    var shortName : String {
        get {
            return self.rawAttributes["shortName"] as! String
        }
    }
    
    var allianceID : Int? {
        get {
            let rawID = self.rawAttributes["allianceID"] as! String
            return Int(rawID)
        }
    }
    
    var executorCorpID : Int? {
        get {
            let rawID = self.rawAttributes["executorCorpID"] as! String
            return Int(rawID)
        }
    }
    
    var memberCount : Int? {
        get {
            let count = self.rawAttributes["memberCount"] as! String
            return Int(count)
        }
    }
    
    var startDate : NSDate? {
        get {
            let date = self.rawAttributes["startDate"] as! String
            return dateStringToNSDate(date)
        }
    }
    
    var memberCorporations : [AllianceMemberCorpRow] {
        get {
            if let rowset = childRowset {
                return rowset.rows as! [AllianceMemberCorpRow]
            } else {
                return []
            }
        }
    }
    
    override var description : String {
        get {
            return self.dynamicType.description() + ".\(name)" + " (\(memberCount!) members)"
        }
    }

}

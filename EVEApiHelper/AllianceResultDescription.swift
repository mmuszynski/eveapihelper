//
//  AllianceResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/22/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class AllianceResultDescription: ResultDescription {
    
    var alliances : [AllianceRow] {
        get {
            if let rowset = rowSets["alliances"] as? AllianceRowset {
                return rowset.alliances
            } else {
                return []
            }
        }
    }
    
    var allCorporations : [CorporationRow] {
        get {
            var returnArray = [CorporationRow]()
            for row in alliances {
                let corps = row.memberCorporations.map { $0.basicCorporationRow }
                returnArray += corps
            }
            return returnArray
        }
    }

}

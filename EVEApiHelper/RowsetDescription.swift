//
//  RowsetDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/14/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class RowsetDescription: NSObject {
    
    //<rowset name="callGroups" key="groupID" columns="groupID,name,description">
    var rawName = ""
    var rawKey = ""
    var rawColumns = [String]()
    
    var rows = [GenericRowDescription]()
    var rowCount : Int {
        get {
            return rows.count
        }
    }
    var parentRow : GenericRowDescription?
    
    func setColumns(columnsString: String) {
        rawColumns = columnsString.componentsSeparatedByString(",")
    }
    
    func addRow(attributesDict attributes: NSDictionary) -> GenericRowDescription? {

        var newRow : GenericRowDescription?
        
        switch self.rawName {
        case "alliances":
            newRow = AllianceRow(attributeDict: attributes)
        case "memberCorporations":
            newRow = AllianceMemberCorpRow(attributeDict: attributes)
        case "callGroups":
            newRow = APICallGroupRow(attributeDict: attributes)
        case "calls":
            newRow = APICallRow(attributeDict: attributes)
        case "characters", "types":
            if self.rawColumns.count == 2 {
                newRow = GenericNameRow(attributeDict: attributes)
            }
        case "starbases":
            newRow = StarbaseListRow(attributeDict: attributes)
        case "fuel":
            newRow = StarbaseFuelRow(attributeDict: attributes)
        case "assets", "contents":
            newRow = AssetListRow(attributeDict: attributes)
        case "locations":
            newRow = LocationsRow(attributeDict: attributes)
        default:
            newRow = GenericRowDescription(attributeDict: attributes)
        }
        
        if let row = newRow {
            row.parentRowset = self
            rows.append(row)
        }
        
        return newRow

    }
    
    override var description : String {
        get {
            let rowType = rows.first?.dynamicType.description() ?? "row"
            return self.dynamicType.description() + ".\(rawName)" + " (\(rawColumns.count) columns, \(rowCount) \(rowType)s)"
        }
    }
    
}

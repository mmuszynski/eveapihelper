//
//  CharacterNameRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/23/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class GenericNameRow: GenericRowDescription {
    
    var name : String? {
        get {
            return rawAttributes["name"] as? String ?? rawAttributes["typeName"] as? String
        }
    }
    
    var id : Int64? {
        get {
            if let theID = rawAttributes["characterID"] as? String {
                return getInt64ForKey("characterID")
            } else if let theID = rawAttributes["typeID"] as? String {
                return getInt64ForKey("typeID")
            }
            return nil
        }
    }

}

//
//  ImageServerFetcher.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/23/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import Cocoa

protocol ImageServerFetcherDelegate {
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: NSImage)
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher)
}

class ImageServerFetcher: NSObject, NSURLSessionDataDelegate {
    
    let assetType : AssetType
    let assetID : Int
    let assetWidth: Int
    var url : NSURL?
    
    var image : NSImage?
    var downloadData = NSMutableData()
    var delegate : ImageServerFetcherDelegate?
    
    enum AssetType : String  {
        case Alliance = "Alliance"
        case Corporation = "Corporation"
        case Character = "Character"
        case TypeID = "Type"
        case Render = "Render"
    }
    
    init(assetType: AssetType, assetID: Int, assetWidth: Int) {
        self.assetType = assetType
        self.assetID = assetID
        self.assetWidth = assetWidth
        super.init()
    }
    
    func fetch() {
        downloadData = NSMutableData()
        let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: self, delegateQueue: nil)
            
        let components = NSURLComponents()
        components.scheme = "https"
        components.host = "image.eveonline.com"
        components.path = "/\(assetType.rawValue)/\(assetID)_\(assetWidth).png"
        if let theUrl = components.URL {
            url = theUrl
            let request = NSURLRequest(URL: theUrl)
            let task = session.dataTaskWithRequest(request)
            task.resume()
        }
        
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        downloadData.appendData(data)
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        if let image = NSImage(data: downloadData) {
            delegate?.imageFetcherDidFinishSuccessfully(self, withImage: image)
        } else {
            delegate?.imageFetcherDidFinishUnsuccessfully(self)
        }
        
    }
    
    
    
}

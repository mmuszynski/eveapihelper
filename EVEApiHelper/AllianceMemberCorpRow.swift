//
//  AllianceMemberCorpRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/18/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class AllianceMemberCorpRow: GenericRowDescription {
    
    var corporationID : Int? {
        get {
            let id = self.rawAttributes["corporationID"] as! String
            return Int(id)
        }
    }
    var startDate : NSDate? {
        get {
            let date = self.rawAttributes["startDate"] as! String
            return dateStringToNSDate(date)
        }
    }
    
    var basicCorporationRow : CorporationRow {
        get {
            return CorporationRow(attributeDict: ["corporationID" : self.corporationID!])
        }
    }

}

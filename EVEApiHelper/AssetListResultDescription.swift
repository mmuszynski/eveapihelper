//
//  AssetListResultDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/27/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class AssetListResultDescription: ResultDescription {
    
    var assetArray: [AssetListRow] {
        get {
            if let assets = rowsetArray.first?.rows as? [AssetListRow] {
                return assets
            } else {
                return []
            }
        }
    }
   
}

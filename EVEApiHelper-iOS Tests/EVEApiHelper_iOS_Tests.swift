//
//  EVEApiHelper_iOSTests.swift
//  EVEApiHelper-iOSTests
//
//  Created by Mike Muszynski on 1/24/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit
import XCTest

class EVEApiHelper_iOSTests: XCTestCase, GenericXMLFetcherDelegate {
    
    var expectParsingComplete : XCTestExpectation!
    var expectParsingFailed : XCTestExpectation!
    var measurementTest = false
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSupportedApiCalls() {
        
        self.measureBlock() {
            let callGroups = EVEApiHelperCall.callGroups()
            let callTypes = EVEApiHelperCall.callTypes()
            var apiCalls = [EVEApiHelperCall]()
            var fetchers = [GenericXMLFetcher]()
            
            var supported = 0
            var unsupported = 0
            
            for callGroup in callGroups {
                for callType in callTypes {
                    let apiCall = EVEApiHelperCall(callGroup: callGroup, callType: callType)
                    if let fetcher = apiCall.fetcher {
                        apiCalls.append(apiCall)
                        fetchers.append(fetcher)
                    }
                }
            }
            XCTAssertTrue(apiCalls.count == 86, "Wrong number of supported API Calls (Found \(supported), expected 86")
        }
        
    }
    
    func testCallList() {
        measurementTest = true
        expectParsingComplete = expectationWithDescription("parsingComplete")

        let apiCall = EVEApiHelperCall(callGroup: .API, callType: .CallList)
        let fetcher = GenericXMLFetcher(apiCall: apiCall, error: nil)
        fetcher?.delegate = self
        fetcher?.start()
    

        self.waitForExpectationsWithTimeout(10.0, handler: nil)
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {}
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        if let result = fetcher.result as? ErrorResultDescription {
            expectParsingFailed.fulfill()
        } else {
            expectParsingComplete.fulfill()
        }
    }
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {}
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {}
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?) {

    }
}

//
//  AlliancesViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/20/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import Cocoa

class AlliancesViewController: NSViewController, GenericXMLFetcherDelegate, NSOutlineViewDataSource, NSOutlineViewDelegate, ImageServerFetcherDelegate {
    
    let allianceFetcher = GenericXMLFetcher(apiCall: EVEApiHelperCall(callGroup: .EVE, callType: .AllianceList), error: nil)
    //let allianceFetcher = GenericXMLFetcher(url: NSURL(fileURLWithPath: "/Volumes/Storage/Users/mike/Desktop/AllianceList.xml.aspx.xml"))
    var alliances = [AllianceRow]()
    var corpList = [CorporationRow]()
    var corpNames = [Int : String]()
    var allianceImages = [Int : NSImage]()
    var itemsWaitingForImages = [Int : AllianceRow]()

    override func viewDidLoad() {
        super.viewDidLoad()
        allianceFetcher?.delegate = self
        
        // Do view setup here.
    }
    
    @IBOutlet var queryButton: NSButton!
    @IBAction func pressedQueryButton(sender: AnyObject) {
        queryButton.enabled = false
        resolveCorpNamesCheckbox.enabled = false
        allianceFetcher?.start()
        progressIndicator.indeterminate = true
        progressIndicator.startAnimation(self)
        progressLabel.stringValue = "Downloading..."
        
    }
    @IBOutlet var resolveCorpNamesCheckbox: NSButton!
    @IBOutlet var progressLabel: NSTextField!
    @IBOutlet var progressIndicator: NSProgressIndicator!
    @IBOutlet var allianceOutlineView: NSOutlineView!
    
    func XMLFetcherDidReportProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        if let maxNum = total {
            
            progressLabel.stringValue = "Parsing Alliance row \(num) of \(maxNum)..."
            progressIndicator.indeterminate = false
            progressIndicator.doubleValue = Double(num)
            progressIndicator.maxValue = Double(maxNum)

        } else {
            let maxNum = corpList.count
            progressLabel.stringValue = "Fetching Corp Name \(num) of \(maxNum)..."
            progressIndicator.indeterminate = false
            progressIndicator.doubleValue = Double(num)
            progressIndicator.maxValue = Double(maxNum)
        }
        
    }
    
    func XMLFetcherDidFinish(fetcher: GenericXMLFetcher) {
        progressIndicator.doubleValue = progressIndicator.maxValue
        progressIndicator.stopAnimation(self)
        
        if let result = fetcher.result {
            if let allianceResult = result as? AllianceResultDescription {
                self.alliances = allianceResult.alliances
                corpList = allianceResult.allCorporations
                let corpIDList = corpList.map { $0.corporationID! }
                
                if resolveCorpNamesCheckbox.state == NSOnState && fetcher.apiCall?.callType != EVEApiHelperCall.CallType.CharacterName {
                    progressLabel.stringValue = "Preparing to fetch Corp Names..."
                    //note here the use of CharacterName even for Corp Names
                    let apiCall = EVEApiHelperCall(callGroup: .EVE, callType: .CharacterName)
                    if let corpFetcher = GenericXMLFetcher(apiCall: apiCall, error: nil) {
                        corpFetcher.idList = corpIDList
                        corpFetcher.delegate = self
                        corpFetcher.start()
                    }
                } else {
                    reloadOutlineView()
                }
            } else if let names = result.rowSets["characters"]?.rows as? [GenericNameRow] {
                for row in names {
                    corpNames[row.id!] = row.name
                }
                
                reloadOutlineView()
            }
            
        }
    }
    
    func reloadOutlineView() {
        dispatch_async(dispatch_get_main_queue(),{
            self.allianceOutlineView.reloadData()
            self.queryButton.enabled = true
            self.progressLabel.stringValue = "Finished."
            self.resolveCorpNamesCheckbox.enabled = false
        });
    }
    
    func outlineView(outlineView: NSOutlineView, numberOfChildrenOfItem item: AnyObject?) -> Int {
        if item == nil {
            return alliances.count
        } else if let row = item as? AllianceRow {
            return row.memberCorporations.count
        }
        
        return 0
    }
    
    func outlineView(outlineView: NSOutlineView, child index: Int, ofItem item: AnyObject?) -> AnyObject {
        if item == nil {
            return alliances[index]
        } else if let row = item as? AllianceRow {
            return row.memberCorporations[index]
        }
        
        return ""
    }
//    
//    func outlineView(outlineView: NSOutlineView, objectValueForTableColumn tableColumn: NSTableColumn?, byItem item: AnyObject?) -> AnyObject? {
//        
//        if let theItem = item as? AllianceRow {
//            
////            if let identifier = tableColumn?.identifier {
////                switch identifier {
////                case "number":
////                    return "\(theItem.memberCount!) members"
////                default:
////                    return theItem.name
////                }
////            } else {
////                return theItem.name
////            }
//            
//        } else if let theItem = item as? AllianceMemberCorpRow {
////            if let identifier = tableColumn?.identifier {
////                switch identifier {
////                case "number":
////                    return "\(theItem.corporationID!)"
////                default:
////                    return corpNames[theItem.corporationID!] ?? "\(theItem.corporationID!)"
////                }
////            }
//        }
//        
//        return "BBBBB"
//    }
    
    func outlineView(outlineView: NSOutlineView, viewForTableColumn tableColumn: NSTableColumn?, item: AnyObject) -> NSView? {
        let cell = outlineView.makeViewWithIdentifier("allianceCell", owner: self) as! AllianceTableCellView
        
        if let theItem = item as? AllianceRow {
            cell.allianceNameLabel.stringValue = "\(theItem.name) - \(theItem.memberCount!) members"
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MM-dd-YYYY"
            let dateString = dateFormatter.stringFromDate(theItem.startDate!)
            cell.allianceStartDateLabel.stringValue = "Established: \(dateString)"
            
            if let image = allianceImages[theItem.allianceID!] {
                cell.allianceLogoView.image = image
                cell.logoDownloadingIndicator.stopAnimation(self)
            } else {
                cell.allianceLogoView.image = nil
                fetchImageFromServerForAllianceItem(theItem)
                cell.logoDownloadingIndicator.startAnimation(self)
            }
        }
        
        return cell
    }
    
    func outlineView(outlineView: NSOutlineView, isItemExpandable item: AnyObject) -> Bool {
        return item is AllianceRow
    }
    
    func fetchImageFromServerForAllianceItem(item: AllianceRow) {
        if let id = item.allianceID {
            itemsWaitingForImages[id] = item
            
            let fetcher = ImageServerFetcher(assetType: .Alliance, assetID: id, assetWidth: 128)
            fetcher.delegate = self
            fetcher.fetch()
        }
    }
    
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: NSImage) {
        allianceImages[fetcher.assetID] = image
        let item = itemsWaitingForImages[fetcher.assetID]
        if itemsWaitingForImages[fetcher.assetID] != nil {
            itemsWaitingForImages.removeValueForKey(fetcher.assetID)
        }
        dispatch_async(dispatch_get_main_queue(), {
            let row = self.allianceOutlineView.rowForItem(item)
            self.allianceOutlineView.reloadDataForRowIndexes(NSIndexSet(index: row), columnIndexes: NSIndexSet(index: 0))
        })
    }
    
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher) {
        fetcher.fetch()
    }
    
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?) {
        
        
    }
    
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        
        
    }
    
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        
    }
    
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {
        
        
    }

}

//
//  EVEApiHelperCall.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/30/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import Foundation

struct EVEApiHelperCall: Hashable {
    
    let callGroup: CallGroup, callType: CallType
    
    var fetcher : GenericXMLFetcher? {
        get {
            if self.supported {
                return try? GenericXMLFetcher(apiCall: self)
            }
            
            return nil
        }
    }
    
    enum CallGroup : String {
        case Account = "Account"
        case Character = "Char"
        case Corporation = "Corp"
        case EVE = "EVE"
        case Map = "Map"
        case Server = "Server"
        case API = "API"
    }
    
    enum CallType : String {
        case AccountStatus = "AccountStatus"
        case APIKeyInfo = "APIKeyInfo"
        case Characters = "Characters"
        case AccountBalance = "AccountBalance"
        case AssetList = "AssetList"
        case BlueprInts = "BlueprInts"
        case CalendarEventAttendees = "CalendarEventAttendees"
        case CharacterSheet = "CharacterSheet"
        case ContactList = "ContactList"
        case ContactNotifications = "ContactNotifications"
        case Contracts = "Contracts"
        case ContractItems = "ContractItems"
        case ContractBids = "ContractBids"
        case FacWarStats = "FacWarStats"
        case IndustryJobs = "IndustryJobs"
        case IndustryJobsHistory = "IndustryJobsHistory"
        case KillMails = "KillMails"
        case Locations = "Locations"
        case MailBodies = "MailBodies"
        case MailingLists = "MailingLists"
        case MailMessages = "MailMessages"
        case MarketOrders = "MarketOrders"
        case Medals = "Medals"
        case Notifications = "Notifications"
        case NotificationTexts = "NotificationTexts"
        case PlanetaryColonies = "PlanetaryColonies"
        case PlanetaryPins = "PlanetaryPins"
        case PlanetaryRoutes = "PlanetaryRoutes"
        case PlanetaryLinks = "PlanetaryLinks"
        case Research = "Research"
        case SkillIntraining = "SkillIntraining"
        case SkillQueue = "SkillQueue"
        case Standings = "Standings"
        case UpcomingCalendarEvents = "UpcomingCalendarEvents"
        case WalletJournal = "WalletJournal"
        case WalletTransactions = "WalletTransactions"
        case ContainerLog = "ContainerLog"
        case CorporationSheet = "CorporationSheet"
        case CustomsOffices = "CustomsOffices"
        case Facilities = "Facilities"
        case MemberMedals = "MemberMedals"
        case MemberSecurity = "MemberSecurity"
        case MemberSecurityLog = "MemberSecurityLog"
        case MemberTracking = "MemberTracking"
        case OutpostList = "OutpostList"
        case OutpostServiceDetail = "OutpostServiceDetail"
        case Shareholders = "Shareholders"
        case StarbaseDetail = "StarbaseDetail"
        case StarbaseList = "StarbaseList"
        case Titles = "Titles"
        case AllianceList = "AllianceList"
        case CertificateTree = "CertificateTree"
        case CharacterAffiliation = "CharacterAffiliation"
        case CharacterID = "CharacterID"
        case CharacterInfo = "CharacterInfo"
        case CharacterName = "CharacterName"
        case ConquerableStationList = "ConquerableStationList"
        case ErrorList = "ErrorList"
        case FacWarTopStats = "FacWarTopStats"
        case RefTypes = "RefTypes"
        case SkillTree = "SkillTree"
        case TypeName = "TypeName"
        case FacWarSystems = "FacWarSystems"
        case Jumps = "Jumps"
        case Kills = "Kills"
        case Sovereignty = "Sovereignty"
        case ServerStatus = "ServerStatus"
        case CallList = "CallList"
    }
    
    var hashValue : Int {
        get {
            return "\(self.callGroup.rawValue),\(self.callType.rawValue)".hashValue
        }
    }
    
    var approximateContentLength : Int? {
        //gives the approximate length of the download in bytes
        get {
            switch self.callType {
            case .AllianceList:
                return 1900000
            default:
                return nil
            }
        }
    }
    
    var supported: Bool {
        get {
            
            switch self.callGroup {
            case .Account:
                switch self.callType {
                case .AccountStatus, .APIKeyInfo, .Characters:
                    return true
                default:
                    return false
                }
                
            case .Character:
                switch self.callType {
                case .AccountBalance,
                .AssetList,
                .BlueprInts,
                .CalendarEventAttendees,
                .CharacterSheet,
                .ContactList,
                .ContactNotifications,
                .Contracts,
                .ContractItems,
                .ContractBids,
                .FacWarStats,
                .IndustryJobs,
                .IndustryJobsHistory,
                .KillMails,
                .Locations,
                .MailBodies,
                .MailingLists,
                .MailMessages,
                .MarketOrders,
                .Medals,
                .Notifications,
                .NotificationTexts,
                .PlanetaryColonies,
                .PlanetaryPins,
                .PlanetaryRoutes,
                .PlanetaryLinks,
                .Research,
                .SkillIntraining,
                .SkillQueue,
                .Standings,
                .UpcomingCalendarEvents,
                .WalletJournal,
                .WalletTransactions:
                    return true
                default:
                    return false
                }
                
            case .Corporation:
                switch self.callType {
                case .AccountBalance,
                .AssetList,
                .BlueprInts,
                .ContactList,
                .ContainerLog,
                .Contracts,
                .ContractItems,
                .ContractBids,
                .CorporationSheet,
                .CustomsOffices,
                .Facilities,
                .FacWarStats,
                .IndustryJobs,
                .IndustryJobsHistory,
                .KillMails,
                .Locations,
                .MarketOrders,
                .Medals,
                .MemberMedals,
                .MemberSecurity,
                .MemberSecurityLog,
                .MemberTracking,
                .OutpostList,
                .OutpostServiceDetail,
                .Shareholders,
                .Standings,
                .StarbaseDetail,
                .StarbaseList,
                .Titles,
                .WalletJournal,
                .WalletTransactions:
                    return true
                default:
                    return false
                }
            case .EVE:
                switch self.callType {
                case .AllianceList,
                .CertificateTree,
                .CharacterAffiliation,
                .CharacterID,
                .CharacterInfo,
                .CharacterName,
                .ConquerableStationList,
                .ErrorList,
                .FacWarStats,
                .FacWarTopStats,
                .RefTypes,
                .SkillTree,
                .TypeName:
                    return true
                default:
                    return false
                }
            case .API:
                switch self.callType {
                case .CallList:
                    return true
                default:
                    return false
                }
            case .Map:
                    switch self.callType {
                    case .FacWarStats, .Jumps, .Kills, .Sovereignty:
                        return true
                    default:
                        return false
                }
            case .Server:
                switch self.callType {
                case .ServerStatus:
                    return true
                default:
                    return false
                }
            default:
                return false
            }
        }
    }

    static func callGroups() -> [EVEApiHelperCall.CallGroup] {
        return [EVEApiHelperCall.CallGroup.Account,
            EVEApiHelperCall.CallGroup.API,
            EVEApiHelperCall.CallGroup.Character,
            EVEApiHelperCall.CallGroup.Corporation,
            EVEApiHelperCall.CallGroup.EVE,
            EVEApiHelperCall.CallGroup.Map,
            EVEApiHelperCall.CallGroup.Server]
    }
    
    static func callTypes() -> [EVEApiHelperCall.CallType] {
        
        return [EVEApiHelperCall.CallType.AccountStatus,
            EVEApiHelperCall.CallType.APIKeyInfo,
            EVEApiHelperCall.CallType.Characters,
            EVEApiHelperCall.CallType.AccountBalance,
            EVEApiHelperCall.CallType.AssetList,
            EVEApiHelperCall.CallType.BlueprInts,
            EVEApiHelperCall.CallType.CalendarEventAttendees,
            EVEApiHelperCall.CallType.CharacterSheet,
            EVEApiHelperCall.CallType.ContactList,
            EVEApiHelperCall.CallType.ContactNotifications,
            EVEApiHelperCall.CallType.Contracts,
            EVEApiHelperCall.CallType.ContractItems,
            EVEApiHelperCall.CallType.ContractBids,
            EVEApiHelperCall.CallType.FacWarStats,
            EVEApiHelperCall.CallType.IndustryJobs,
            EVEApiHelperCall.CallType.IndustryJobsHistory,
            EVEApiHelperCall.CallType.KillMails,
            EVEApiHelperCall.CallType.Locations,
            EVEApiHelperCall.CallType.MailBodies,
            EVEApiHelperCall.CallType.MailingLists,
            EVEApiHelperCall.CallType.MailMessages,
            EVEApiHelperCall.CallType.MarketOrders,
            EVEApiHelperCall.CallType.Medals,
            EVEApiHelperCall.CallType.Notifications,
            EVEApiHelperCall.CallType.NotificationTexts,
            EVEApiHelperCall.CallType.PlanetaryColonies,
            EVEApiHelperCall.CallType.PlanetaryPins,
            EVEApiHelperCall.CallType.PlanetaryRoutes,
            EVEApiHelperCall.CallType.PlanetaryLinks,
            EVEApiHelperCall.CallType.Research,
            EVEApiHelperCall.CallType.SkillIntraining,
            EVEApiHelperCall.CallType.SkillQueue,
            EVEApiHelperCall.CallType.Standings,
            EVEApiHelperCall.CallType.UpcomingCalendarEvents,
            EVEApiHelperCall.CallType.WalletJournal,
            EVEApiHelperCall.CallType.WalletTransactions,
            EVEApiHelperCall.CallType.ContainerLog,
            EVEApiHelperCall.CallType.CorporationSheet,
            EVEApiHelperCall.CallType.CustomsOffices,
            EVEApiHelperCall.CallType.Facilities,
            EVEApiHelperCall.CallType.MemberMedals,
            EVEApiHelperCall.CallType.MemberSecurity,
            EVEApiHelperCall.CallType.MemberSecurityLog,
            EVEApiHelperCall.CallType.MemberTracking,
            EVEApiHelperCall.CallType.OutpostList,
            EVEApiHelperCall.CallType.OutpostServiceDetail,
            EVEApiHelperCall.CallType.Shareholders,
            EVEApiHelperCall.CallType.StarbaseDetail,
            EVEApiHelperCall.CallType.StarbaseList,
            EVEApiHelperCall.CallType.Titles,
            EVEApiHelperCall.CallType.AllianceList,
            EVEApiHelperCall.CallType.CertificateTree,
            EVEApiHelperCall.CallType.CharacterAffiliation,
            EVEApiHelperCall.CallType.CharacterID,
            EVEApiHelperCall.CallType.CharacterInfo,
            EVEApiHelperCall.CallType.CharacterName,
            EVEApiHelperCall.CallType.ConquerableStationList,
            EVEApiHelperCall.CallType.ErrorList,
            EVEApiHelperCall.CallType.FacWarTopStats,
            EVEApiHelperCall.CallType.RefTypes,
            EVEApiHelperCall.CallType.SkillTree,
            EVEApiHelperCall.CallType.TypeName,
            EVEApiHelperCall.CallType.FacWarSystems,
            EVEApiHelperCall.CallType.Jumps,
            EVEApiHelperCall.CallType.Kills,
            EVEApiHelperCall.CallType.Sovereignty,
            EVEApiHelperCall.CallType.ServerStatus,
            EVEApiHelperCall.CallType.CallList]
    }
    
}

func ==(lhs: EVEApiHelperCall, rhs: EVEApiHelperCall) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

//
//  GenericRowDescription.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/14/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class GenericRowDescription: NSObject {

    var childRowset : RowsetDescription?
    var parentRowset : RowsetDescription?

    var rawAttributes = NSDictionary()
    
    init(attributeDict: NSDictionary) {
        super.init()
        self.rawAttributes = attributeDict
    }
    
    func getStringForKey(key: String) -> String? {
        return rawAttributes[key] as? String
    }
    
    func getIntForKey(key: String) -> Int? {
        let theInt = getStringForKey(key)
        return Int(theInt?)
    }
    
    func getInt64ForKey(key: String) -> Int64? {
        if let theInt = getStringForKey(key) {
            return NSNumberFormatter().numberFromString(theInt)?.longLongValue
        }
        return nil
    }
    
    func getDateForKey(key: String) -> NSDate? {
        if let dateString = getStringForKey(key) {
            return dateStringToNSDate(dateString)
        }
        
        return nil
    }
    
    func getDoubleForKey(key: String) -> Double? {
        if let theDouble = getStringForKey(key) {
            return NSNumberFormatter().numberFromString(theDouble)?.doubleValue
        }
        
        return nil
    }
}

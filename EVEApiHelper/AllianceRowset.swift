//
//  AllianceRowset.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/22/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif
class AllianceRowset: RowsetDescription {
    
    var alliances : [AllianceRow] {
        get {
            return rows as! [AllianceRow]
        }
    }

}

//
//  GenericXMLFetcher.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/12/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
import Cocoa
#elseif os(iOS)
import UIKit
#endif

protocol GenericXMLFetcherDelegate {
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?)
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher)
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher)
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?)
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?)
}

class GenericXMLFetcher: NSObject, NSXMLParserDelegate, NSURLSessionDataDelegate {
    var delegate : GenericXMLFetcherDelegate?
    var totalLines : Int?
    
    var parser : NSXMLParser!
    var session : NSURLSession!
    var url : NSURL!
    var expectedDataLength : Int?
    var downloadData : NSMutableData!
    var result : ResultDescription!
    
    var useCache = true
    
    var resultAttributes = [String : String]()
    var currentText = ""
    var rowsets = [String : RowsetDescription]()

    var currentRow : GenericRowDescription?
    var currentRowSet : RowsetDescription?
    
    var apiKey : ApiKey?
    var idList : [Int64]?
    var itemID : Int64?
    var attributes : [String : AnyObject]?
    
    var apiCall : EVEApiHelperCall?
    
    override init() {
        fatalError("This class should not be initialized without using convenience method init(url:) or init(apiCall:error:)")
    }
    
    init?(url: NSURL?) {
        self.url = url
        super.init()
        
        if url == nil {
            return nil
        }
    }
    
    init(apiCall: EVEApiHelperCall) throws {
        let baseURL = NSURL(string: "https://api.eveonline.com/")
        let fullURL = baseURL?.URLByAppendingPathComponent("\(apiCall.callGroup.rawValue)/\(apiCall.callType.rawValue).xml.aspx")
        self.url = fullURL
        self.apiCall = apiCall
        super.init()
        
        //if this is an unspported api call, return nil
        if !apiCall.supported {
            throw NSError(domain: "EVEApiHelperErrorDomain", code: -1, userInfo: [NSLocalizedDescriptionKey : "API Call Not Supported", NSLocalizedFailureReasonErrorKey : "The selected API Configuration is invalid"])
        }
    }
    
    func start() {
        
        var components = NSURLComponents(URL: url!, resolvingAgainstBaseURL: false)
        var queryItems = [NSURLQueryItem]()
        
        //if there are ids, list them as attributes
        if let ids = idList {
            var idString = ""
            var limit = 200
            if ids.count < limit {
                limit = ids.count
            }
            
            for i in 0..<limit {
                idString += "\(ids[i])"
                if i+1 < limit {
                    idString += ","
                }
            }
            
            idList!.removeRange( 0..<limit )
            queryItems.append(NSURLQueryItem(name: "ids", value: idString))
            
        } else if itemID != nil {
            let idString = "\(itemID!)"
            queryItems.append(NSURLQueryItem(name: "itemID", value: idString))
        }
        
        //if there are other attributes, list them as attributes
        if let urlAttributes = attributes {
            var queryAttributes = urlAttributes.keys.array.map { NSURLQueryItem(name: $0, value: "\(urlAttributes[$0]!)") }
            queryItems += queryAttributes
        }
        
        
        if let key = apiKey {
            queryItems.append( NSURLQueryItem(name: "keyID", value: "\(key.keyID)"))
            queryItems.append( NSURLQueryItem(name: "vCode", value: key.vCode))
        }
        
        components?.queryItems = queryItems
        
        //set the url
        self.url = components?.URL
        
        if useCache, let cachedData = loadCachedFileForURLString(url.absoluteString) {
            
            downloadData = cachedData
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
                self.beginParsing()
            })
            
        } else {
        
            session = NSURLSession(
                configuration: NSURLSessionConfiguration.defaultSessionConfiguration(),
                delegate: self,
                delegateQueue: nil)
            let request = NSMutableURLRequest(URL: url)

            let task = session.dataTaskWithRequest(request)
            downloadData = NSMutableData()
            task.resume()
            
        }
        
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveResponse response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void) {
        delegate?.XMLFetcherDidStartDownloading(self)
        if response.expectedContentLength != -1 {
            expectedDataLength = Int(response.expectedContentLength)
        } else {
            expectedDataLength = apiCall?.approximateContentLength
        }
        completionHandler(.Allow)
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        downloadData.appendData(data)
        delegate?.XMLFetcherDidReportDownloadProgress(self, progress: downloadData.length, ofTotal: expectedDataLength)
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        delegate?.XMLFetcherDidFinishDownloading(self, withError: error)
        beginParsing()
    }
    
    func beginParsing() {
        parser = NSXMLParser(data: self.downloadData)
        parser.delegate = self
        totalLines = NSString(data: self.downloadData, encoding: NSUTF8StringEncoding)?.componentsSeparatedByString("\n").count
        parser.parse()
    }
    
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
    }
    
    func parserDidStartDocument(parser: NSXMLParser) {
        
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if totalLines > 1000 {
            delegate?.XMLFetcherDidReportParsingProgress(self, progress: parser.lineNumber, ofTotal: totalLines)
        }
        
        switch elementName {
            case "rowset":
                parseRowsetWithAttributes(attributeDict)
            case "row":
                parseRowWithAttributes(attributeDict)
            case "error":
                //<error code="106">Text Description</error>
                if let code = attributeDict["code"] as? String {
                    resultAttributes["errorCode"] = code
                }
            case "eveapi":
                if let version = attributeDict["version"] as? String {
                    resultAttributes["apiVersion"] = version
                }
            default:
                1==0
        }
        currentText = ""
    }
    
    func parseRowsetWithAttributes(attributeDict: [NSObject : AnyObject]!) {

        var newRowset : RowsetDescription
        let name = attributeDict["name"] as! String
        
        switch name {
        case "alliances":
            newRowset = AllianceRowset()
        default:
            newRowset = RowsetDescription()
        }
        
        currentRowSet = newRowset
        currentRowSet?.rawName = name ?? ""

        if let columnString = attributeDict["columns"] as? String {
            currentRowSet?.setColumns(columnString)
        }
        
        if let key = attributeDict["key"] as? String {
            currentRowSet?.rawKey = key
        }
        
        if currentRow != nil {
            newRowset.parentRow = currentRow
            currentRow!.childRowset = newRowset
        } else {
            if let rowsetName = currentRowSet?.rawName {
                if let existingRow = rowsets[rowsetName] {
                    //hopefully, if there is already a rowset with this name, we can grab that and ignore the rest of what we have done so far
                    currentRowSet = existingRow
                } else {
                    rowsets[rowsetName] = currentRowSet!
                }
            }
        }
    }
    
    func parseRowWithAttributes(attributeDict: [NSObject : AnyObject]!) {
        currentRow = currentRowSet?.addRow(attributesDict: attributeDict)
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String?) {
        currentText += string!
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case "row":
            currentRowSet = currentRow?.parentRowset
            currentRow = currentRowSet?.parentRow
        case "rowset":
            currentRow = currentRowSet?.parentRow
            currentRowSet = currentRow?.parentRowset
        default:
            if currentText != "" && currentText != "\n" {
                resultAttributes[elementName] = currentText
            }
        }
        
        currentText = ""
        
    }
    
    func parserDidEndDocument(parser: NSXMLParser) {
        if idList?.count > 0 {
            if let charRowset = rowsets["characters"] {
                delegate?.XMLFetcherDidReportParsingProgress(self, progress: charRowset.rowCount, ofTotal: nil)
            }
            self.start()
            return
        }
        
        if itemID != nil {
            resultAttributes["itemID"] = "\(itemID!)"
        }
        
        if resultAttributes["error"] != nil {
            result = ErrorResultDescription(attributeDict: resultAttributes)
            if let urlString = self.url.absoluteString {
                result.urlString = urlString
            }
            //bail out here because of an error. don't do any of the caching
            delegate?.XMLFetcherDidFinishParsing(self)
            return
        } else {
            if rowsets["alliances"] != nil {
                result = AllianceResultDescription(attributeDict: resultAttributes)
            } else if apiCall?.callType == EVEApiHelperCall.CallType.CorporationSheet {
                result = CorporationSheetResultDescription(attributeDict: resultAttributes)
            } else if apiCall?.callType == EVEApiHelperCall.CallType.AccountStatus {
                result = AccountStatusResultDescription(attributeDict: resultAttributes)
            } else if apiCall?.callType == EVEApiHelperCall.CallType.StarbaseList {
                result = StarbaseListResultDescription(attributeDict: resultAttributes)
            } else if apiCall?.callType == EVEApiHelperCall.CallType.StarbaseDetail {
                result = StarbaseDetailResultDescription(attributeDict: resultAttributes)
            } else if apiCall?.callType == EVEApiHelperCall.CallType.AssetList {
                result = AssetListResultDescription(attributeDict: resultAttributes)
            }  else if apiCall?.callType == EVEApiHelperCall.CallType.Locations {
                result = LocationsResultDescription(attributeDict: resultAttributes)
            } else {
                result = ResultDescription(attributeDict: resultAttributes)
            }
            
            result.rowSets = self.rowsets
        }
        
        if let urlString = self.url.absoluteString {
            result.urlString = urlString
        }
        
        result.apiCall = self.apiCall
        
        markCacheTimeForResult(result)
        saveDownloadDataToCacheForResult(result)
        delegate?.XMLFetcherDidFinishParsing(self)
    }
    
    func markCacheTimeForResult(result: ResultDescription) {
        let url = result.urlString
        let cacheDate = result.cachedUntil
        NSUserDefaults.standardUserDefaults().setObject(cacheDate, forKey: url)
    }
    
    func saveDownloadDataToCacheForResult(result: ResultDescription) {
        let apiCall = result.apiCall
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyyddMMHHmmss"
        
        if let call = apiCall {
            if let location = cacheLocationForURLString(result.urlString) {
                downloadData.writeToURL(location, atomically: true)
            }
        }

    }
    
    func cacheLocationForURLString(urlString: String) -> NSURL? {
        //get the cache date
        let path = urlString
        if let cacheDate = NSUserDefaults.standardUserDefaults().objectForKey(path) as? NSDate {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyyddMMHHmmss"
            let dateString = dateFormatter.stringFromDate(cacheDate)
            let hashString = abs(path.hash).description
            
            return applicationSupportDirectory().URLByAppendingPathComponent("\(hashString)-\(dateString).xml")
        }
        
        return nil
    }
    
    func loadCachedFileForURLString(urlString: String) -> NSMutableData? {
        let path = urlString
        if let cacheDate = NSUserDefaults.standardUserDefaults().objectForKey(path) as? NSDate {
            if NSDate() < cacheDate {
                if let url = cacheLocationForURLString(urlString) {
                    return try? NSMutableData(contentsOfURL: url, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                }
            }
        }
        return nil
    }
    
    func applicationSupportDirectory() -> NSURL {
        var error : NSError?
        let applicationSupport: NSURL?
        do {
            applicationSupport = try NSFileManager.defaultManager().URLForDirectory(NSSearchPathDirectory.ApplicationSupportDirectory, inDomain: NSSearchPathDomainMask.UserDomainMask, appropriateForURL: nil, create: true)
        } catch let error1 as NSError {
            error = error1
            applicationSupport = nil
        }
        let specificDirectory = applicationSupport!.URLByAppendingPathComponent("com.mmuszynski.EVEApiHelper", isDirectory: true)
        
        if !NSFileManager.defaultManager().fileExistsAtPath(specificDirectory.path!) {
            do {
                try NSFileManager.defaultManager().createDirectoryAtURL(specificDirectory, withIntermediateDirectories: true, attributes: nil)
            } catch let error1 as NSError {
                error = error1
            }
        }
        
        return specificDirectory
        
    }
    
}

func >(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.earlierDate(rhs) == rhs
}

func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.earlierDate(rhs) == lhs
}

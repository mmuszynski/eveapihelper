//
//  AllianceTableCellView.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/23/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import Cocoa

class AllianceTableCellView: NSTableCellView {
    
    @IBOutlet var allianceLogoView : NSImageView!
    @IBOutlet var allianceNameLabel : NSTextField!
    @IBOutlet var allianceStartDateLabel : NSTextField!
    var logoDownloadingIndicator : NSProgressIndicator!

    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)

        // Drawing code here.
    }
    
    override func awakeFromNib() {
        if logoDownloadingIndicator == nil {
            logoDownloadingIndicator = NSProgressIndicator(frame: allianceLogoView.frame)
            logoDownloadingIndicator.displayedWhenStopped = false
            self.addSubview(logoDownloadingIndicator)
            logoDownloadingIndicator.style = .SpinningStyle
            logoDownloadingIndicator.startAnimation(self)
            
        }
    }
    
}

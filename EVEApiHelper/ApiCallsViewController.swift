//
//  ApiCallsViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/20/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import Cocoa

class ApiCallsViewController: NSViewController, GenericXMLFetcherDelegate {
    
    var apiCalls = [APICallRow]()
    var apiCallGroups = [APICallGroupRow]()

    @IBOutlet weak var progressBar: NSProgressIndicator!
    @IBOutlet weak var progressLabel: NSTextField!
    @IBOutlet weak var callGroupPopUp: NSPopUpButton!
    @IBOutlet weak var apiCallPopUp: NSPopUpButton!

    @IBOutlet var apiDescriptionTextView: NSTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        callGroupPopUp.removeAllItems()
        apiCallPopUp.removeAllItems()
        
        //if let url = NSURL(string: "https://api.eveonline.com/API/CallList.xml.aspx") {
        
        var error : NSError?
        if let fetcher = GenericXMLFetcher(apiCall: EVEApiHelperCall(callGroup: .API, callType: .CallList), error: &error) {
            fetcher.delegate = self
            progressLabel.stringValue = "Downloading Basic API Info..."
            progressBar.indeterminate = true
            progressBar.startAnimation(self)
            fetcher.start()
        } else {
            prIntln(error)
        }
        
    }
    
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError error: NSError?) {
        
        
    }
    
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        
        
    }
    
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        
    }
    
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {
        
        
    }

    func XMLFetcherDidReportProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        progressLabel.stringValue = "Parsing..."
        progressBar.indeterminate = false
        
        self.progressBar.doubleValue = Double(num)
        self.progressBar.maxValue = Double(total!)
    }
    
    func XMLFetcherDidFinish(fetcher: GenericXMLFetcher) {
        
        if let fetchedCallGroups = fetcher.rowsets["callGroups"]?.rows as? [APICallGroupRow] {
            apiCallGroups = fetchedCallGroups
        }
        if let fetchedCalls = fetcher.rowsets["calls"]?.rows as? [APICallRow] {
            apiCalls = fetchedCalls
        }
        
        progressLabel.stringValue = "Finished Downloading Basic API Info"
        progressBar.doubleValue = progressBar.maxValue
        
        callGroupPopUp.addItemsWithTitles(apiCallGroups.map {$0.name} )
        apiCallPopUp.addItemsWithTitles(apiCalls.map { "\($0.name) - \($0.type)"} )
    }
    
    @IBAction func popUpChangedValue(sender: AnyObject) {
        
        if sender as! NSObject == callGroupPopUp {
            
            let callGroupName = callGroupPopUp.titleOfSelectedItem
            let callGroupID = apiCallGroups.filter { $0.name == callGroupName }.first?.groupID
            
            apiCallPopUp.removeAllItems()
            apiCallPopUp.addItemsWithTitles(apiCalls.filter( { $0.groupID == callGroupID } ).map { "\($0.name) - \($0.type)" })
            
        }
        
        var description = "No Description Found"
        
        if let callParts = apiCallPopUp.titleOfSelectedItem?.componentsSeparatedByString(" - ") {
            let callName = callParts[0]
            let callScope = callParts[1]
            let call = apiCalls.filter( { $0.name == callName && $0.type == callScope } ).first
            description = call?.desc ?? "No Description Found"
        }
        
        apiDescriptionTextView.string = description
        
        
    }
}

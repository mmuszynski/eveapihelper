//
//  APICallGroupRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/19/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
import Cocoa
#elseif os(iOS)
import UIKit
#endif

class APICallGroupRow: GenericRowDescription {
    
    var groupID : Int? {
        get {
            let groupID = self.rawAttributes["groupID"] as! String
            return Int(groupID)
        }
    }
    
    var name : String {
        get {
            return self.rawAttributes["name"] as! String
        }
    }
    
    var desc : String {
        get {
            return self.rawAttributes["description"] as! String
        }
    }

}

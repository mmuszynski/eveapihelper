//
//  AllianceTableViewController.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/24/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

import UIKit

class AllianceTableViewController: UITableViewController, GenericXMLFetcherDelegate, ImageServerFetcherDelegate, UISearchResultsUpdating, UISearchControllerDelegate {
    
    @IBOutlet weak var progressView: UIProgressView!
    
    var allianceFetcher = GenericXMLFetcher(apiCall: EVEApiHelperCall(callGroup: .EVE, callType: .AllianceList), error: nil)
    var allianceList : [AllianceRow] = [AllianceRow]() {
        didSet {
            self.filteredAllianceList = self.allianceList
        }
    }
    var allianceImages = [Int : UIImage]()
    var allianceIDtoRowIndex = [Int : NSIndexPath]()
    var searchTerm : String = "" {
        didSet {
            if searchTerm == "" {
                filteredAllianceList = allianceList
            } else {
                let term = self.searchTerm.lowercaseString
                filteredAllianceList = allianceList.filter { $0.name.lowercaseString.rangeOfString(term) != nil || $0.shortName.lowercaseString.rangeOfString(term) != nil }
            }
        }
    }
    
    var filteredAllianceList = [AllianceRow]()
    var allianceSearchController : UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressView.progress = 0
        
        if allianceFetcher != nil {
            allianceFetcher!.delegate = self
        }
        
        self.allianceSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchBar.placeholder = "Search Name or Ticker"
            controller.searchResultsUpdater = self
            controller.searchBar.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(self.tableView.frame), 44.0)
            controller.dimsBackgroundDuringPresentation = false
            controller.hidesNavigationBarDuringPresentation = false
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        allianceSearchController.delegate = self
        
        let button = UIButton(frame: tableView.bounds)
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        button.setTitle("Tap anywhere to start", forState: .Normal)
        button.setTitle("Starting...", forState: .Disabled)
        button.addTarget(self, action: "startFetcher:", forControlEvents: .TouchUpInside)
        self.tableView.backgroundView = button
        
        self.tableView.contentOffset = CGPoIntMake(0.0, 44.0);
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func startFetcher(sender: AnyObject?) {
        allianceFetcher?.start()
        if let button = sender as? UIButton {
            button.enabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - XML Fetcher Delegate Methods
    
    func XMLFetcherDidStartDownloading(fetcher: GenericXMLFetcher) {
        dispatch_async(dispatch_get_main_queue(), {
            self.navigationController?.topViewController.title = "Downloading..."
            self.progressView.setProgress(0, animated: true)
        })
    }
    
    func XMLFetcherDidReportDownloadProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        dispatch_async(dispatch_get_main_queue(), {
            if let theTotal = total {
                let progress = Float(num) / Float(theTotal)
                self.progressView.progress = progress
            } else {
                self.progressView.setProgress(0.95, animated: true)
            }
        })
    }
    
    func XMLFetcherDidFinishDownloading(fetcher: GenericXMLFetcher, withError: NSError?) {
        dispatch_async(dispatch_get_main_queue(), {
            self.progressView.setProgress(1.0, animated: false)
        })
    }
    
    func XMLFetcherDidFinishParsing(fetcher: GenericXMLFetcher) {
        
        if let result = fetcher.result {
            if let allianceResult = result as? AllianceResultDescription {
                self.allianceList = allianceResult.alliances
            }
        }
        dispatch_async(dispatch_get_main_queue(), {
            self.navigationController?.topViewController.title = "Alliance List"
            self.progressView.setProgress(1.0, animated: true)
            self.progressView.hidden = true
            self.tableView.reloadData()
            self.tableView.scrollEnabled = true
        })
    }
    
    func XMLFetcherDidReportParsingProgress(fetcher: GenericXMLFetcher, progress num: Int, ofTotal total: Int?) {
        
        dispatch_async(dispatch_get_main_queue(), {
            self.navigationController?.topViewController.title = "Parsing..."

            if let theTotal = total {
                let progress = Float(num) / Float(theTotal)
                self.progressView.progress = progress
            }
        })
        
    }

    // MARK: - Table view data source

    override func numberOfSectionsIntableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return filteredAllianceList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("allianceCell") as! AllianceTableViewCell
        
        let index = indexPath.row
        let alliance = filteredAllianceList[index]
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-YYYY"
        let startDateString = dateFormatter.stringFromDate(alliance.startDate!)
        
        let membership = alliance.memberCount
        let membershipString = "\(membership!) " + simplePluralize("member", membership!)
        
        cell.allianceNameLabel.text = alliance.name
        cell.allianceTickerLabel.text = "[\(alliance.shortName)]"
        cell.allianceDateLabel.text = "Established: \(startDateString)"
        cell.allianceNumbersLabel.text = membershipString
        
        allianceIDtoRowIndex[alliance.allianceID!] = indexPath
        
        if let image = allianceImages[alliance.allianceID!] {
            cell.allianceImageView.image = image
            cell.spinner.stopAnimating()
        } else {
            cell.allianceImageView.image = nil
            fetchImageForAllianceItemID(alliance.allianceID!)
            cell.spinner.startAnimating()
        }

        return cell
    }
    
    func fetchImageForAllianceItemID(itemID: Int) {
        let imageFetcher = ImageServerFetcher(assetType: .Alliance, assetID: itemID, assetWidth: 128)
        imageFetcher.delegate = self
        imageFetcher.fetch()
    }
    
    func imageFetcherDidFinishSuccessfully(fetcher: ImageServerFetcher, withImage image: UIImage) {
        let id = fetcher.assetID
        allianceImages[id] = image
        if let index = allianceIDtoRowIndex[id] {
            dispatch_async(dispatch_get_main_queue(), {
                if self.searchTerm == "" {
                    self.tableView.reloadRowsAtIndexPaths([index], withRowAnimation: UITableViewRowAnimation.None)
                } else {
                    self.tableView.reloadData()
                }
            })
        } else {
            prIntln("couldn't find row index")
        }
        
    }
    
    func imageFetcherDidFinishUnsuccessfully(fetcher: ImageServerFetcher) {
        prIntln("unsuccessful")
    }
    

    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let text = searchController.searchBar.text
        searchTerm = text
        tableView.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let detailController = segue.destinationViewController as? AllianceDetailViewController {
            if let cell = sender as? UITableViewCell {
                let index = tableView.indexPathForCell(cell)
                let row = index?.row
                if row != nil {
                    let alliance = filteredAllianceList[row!]
                    detailController.alliance = alliance
                    detailController.allianceImage = allianceImages[alliance.allianceID!]
                }
            }
        }
        
        allianceSearchController.dismissViewControllerAnimated(true, completion: nil)
    
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }

}

func simplePluralize(string: String, number: Int) -> String {
    if number == 1 {
        return string
    } else {
        return string + "s"
    }
}

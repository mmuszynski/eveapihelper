//
//  CorporationRow.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/22/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
import Cocoa
#elseif os(iOS)
import UIKit
#endif

class CorporationRow: GenericRowDescription {
    
    var corporationID : Int? {
        get {
            let theInt = rawAttributes["corporationID"] as? String
            return Int(theInt?)
        }
    }
    var corporationName : String {
        get {
            return rawAttributes["corporationName"] as! String
        }
    }
    var ticker : String {
        get {
            return rawAttributes["ticker"] as! String
        }
    }
    var ceoID : Int? {
        get {
            let theInt = rawAttributes["ceoID"] as! String
            return Int(theInt)
        }
    }
    var ceoName : String {
        get {
            return rawAttributes["ceoName"] as! String
        }
    }
    var stationID : Int? {
        get {
            let theInt = rawAttributes["stationID"] as! String
            return Int(theInt)
        }
    }
    var stationName : String {
        get {
            return rawAttributes["stationName"] as! String
        }
    }
    var desc : String {
        get {
            return rawAttributes["description"] as! String
        }
    }
    var url : String? {
        get {
            return rawAttributes["url"] as? String
        }
    }
    var allianceID : Int? {
        get {
            let theInt = rawAttributes["allianceID"] as! String
            return Int(theInt)
        }
    }
    var allianceName : String {
        get {
            return rawAttributes["allianceName"] as! String
        }
    }
    var taxRate : String {
        get {
            return rawAttributes["taxRate"] as! String
        }
    }
    var memberCount : Int? {
        get {
            let theInt = rawAttributes["memberCount"] as! String
            return Int(theInt)
        }
    }
    var memberLimit : Int? {
        get {
            let theInt = rawAttributes["memberLimit"] as? String
            return Int(theInt?)
        }
    }
    var shares : Int? {
        get {
            let theInt = rawAttributes["shares"] as! String
            return Int(theInt)
        }
    }

}

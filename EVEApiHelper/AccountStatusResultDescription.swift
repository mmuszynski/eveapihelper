//
//  AccountStatusResult.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 2/1/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
    #elseif os(iOS)
    import UIKit
#endif

class AccountStatusResultDescription: ResultDescription {
    
    var createDate: NSDate? {
        get {
            let dateString = attributeDictionary["createDate"] as! String
            return NSDateFromEVEDateString(dateString)
        }
    }
   
    var logonCount: Int? {
        get {
            let string = attributeDictionary["logonCount"] as! String
            return Int(string)
        }
    }
    
    var logonMinutes: Int? {
        get {
            let string = attributeDictionary["logonMinutes"] as! String
            return Int(string)
        }
    }
    
    var paidUntil: NSDate? {
        get {
            let string = attributeDictionary["paidUntil"] as! String
            return NSDateFromEVEDateString(string)
        }
    }
}

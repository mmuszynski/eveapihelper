//
//  ApiKey.swift
//  EVEApiHelper
//
//  Created by Mike Muszynski on 1/12/15.
//  Copyright (c) 2015 Mike Muszynski. All rights reserved.
//

#if os(OSX)
    import Cocoa
#elseif os(iOS)
    import UIKit
#endif

class ApiKey: NSObject {
    
    var keyID : UInt = 0
    var vCode : String = ""

    override init() {
        self.keyID = 0
        self.vCode = ""
        super.init()
    }
    
    init(keyId: UInt, vCode: String) {
        self.keyID = keyId
        self.vCode = vCode
        super.init()
    }
    
    init?(secretArrayIndex: Int) {
        super.init()
        if let url = NSBundle.mainBundle().URLForResource("Secrets", withExtension: "plist") {
            if let apiKeyInfo = NSArray(contentsOfURL: url) {
                if let keyInfo = apiKeyInfo[0] as? NSDictionary {
                    self.keyID = keyInfo["keyID"] as! UInt
                    self.vCode = keyInfo["vCode"] as! String
                }
            }
        }
        
        if self.keyID == 0 {
            return nil
        }
        
    }
    
}
